import numpy as np
from IntCode import IntCode

def main():
    lista = get_input()
    ic = IntCode(lista)
    output = ic.execute([5])
    print(output)


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        for k in syote.split(","):
            try:
                lista.append(int(k))
            except ValueError:
                pass
    return lista


if __name__ == "__main__":
    main()
