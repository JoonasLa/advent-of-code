import numpy as np


def main():
    lista = get_input()
    tot = 0
    for l in lista:
        l2 = l // 3 - 2
        while l2 > 0:
            tot += l2
            l2 = l2 // 3 - 2

    print(tot)




def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(int(syote))
    return lista


if __name__ == "__main__":
    main()
