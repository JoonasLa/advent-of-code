import numpy as np


def main():
    lista = get_input()
    koodi = list(lista)
    koodi[1] = 12
    koodi[2] = 2
    i = 0
    while True:
        if koodi[i] == 99:
            break
        if koodi[i] == 1:
            koodi[koodi[i+3]] = koodi[koodi[i+1]] + koodi[koodi[i+2]]
        if koodi[i] == 2:
            koodi[koodi[i + 3]] = koodi[koodi[i + 1]] * koodi[koodi[i + 2]]
        i += 4
    print(koodi)

    for noun in range(100):
        for verb in range(100):
            koodi = list(lista)
            koodi[1] = noun
            koodi[2] = verb
            i = 0
            while True:
                if koodi[i] == 99:
                    break
                if koodi[i] == 1:
                    koodi[koodi[i + 3]] = koodi[koodi[i + 1]] + koodi[koodi[i + 2]]
                if koodi[i] == 2:
                    koodi[koodi[i + 3]] = koodi[koodi[i + 1]] * koodi[koodi[i + 2]]
                i += 4
            if koodi[0] == 19690720:
                print(noun*100+verb)
                return


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        for k in syote.split(","):
            lista.append(int(k))
    return lista


if __name__ == "__main__":
    main()
