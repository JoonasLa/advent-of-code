import numpy as np


def main():
    pi = (128392, 643281)
    tot = 0
    for i in range(pi[0], pi[1]+1):
        istr = str(i)
        pari = False
        incr = True
        for ii in range(len(istr)-1):
            if int(istr[ii]) > int(istr[ii+1]):
                incr = False
                break
        for n in range(10):
            t = 0
            for ii in range(len(istr)):
                if istr[ii] == str(n):
                    t += 1
            if t == 2:
                pari = True
                break
        if pari and incr:
            tot += 1
    print(tot)


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        l = list()
        for k in syote.split(","):
            l.append((k[0], int(k[1:])))
        lista.append(l)
    return lista


if __name__ == "__main__":
    main()
