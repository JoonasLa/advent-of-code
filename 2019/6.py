import numpy as np
from IntCode import IntCode

def main():
    lista = get_input()
    orb = dict()
    for a1, a2 in lista:
        orb[a2] = a1

    tot = 0
    for a2 in orb:
        tot += 1
        a1 = orb[a2]
        while a1 in orb:
            tot += 1
            a1 = orb[a1]
    print(tot)

    l1 = list()
    a1 = "YOU"
    while a1 in orb:
        l1.append(orb[a1])
        a1 = orb[a1]
    print(l1)

    a1 = "SAN"
    tot2 = 0
    while a1 in orb:
        if a1 in l1:
            for k in l1:
                if k == a1:
                    break
                tot2 += 1
            break
        tot2 += 1

        a1 = orb[a1]
    print(tot2)

def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        a1, a2 = syote.split(")")
        lista.append((a1, a2))
    return lista


if __name__ == "__main__":
    main()
