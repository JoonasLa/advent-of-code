import numpy as np


def main():
    w1, w2 = get_input()
    print(w1, w2)

    rist =  dict()
    pist = dict()
    td = 0
    x, y = 0, 0
    suunt = {"U": (0, 1), "D": (0, -1), "L":(-1, 0), "R":(1, 0)}
    for s, d in w1:
        dx, dy = suunt[s]
        for _ in range(d):
            td += 1
            x += dx
            y += dy
            if (x, y) not in pist:
                pist[(x, y)] = td

    x, y = 0, 0
    td = 0
    for s, d in w2:
        dx, dy = suunt[s]
        for _ in range(d):
            td += 1
            x += dx
            y += dy
            if (x, y) in pist and (x, y) not in rist:
                rist[(x, y)] = (pist[(x, y)], td)


    mind = np.inf
    print(rist)
    for x, y in rist:
        if abs(x) + abs(y) < mind:
            mind = abs(x) + abs(y)
    print(mind)

    mind = np.inf
    for key in rist:
        if rist[key][0] + rist[key][1] < mind:
            mind = rist[key][0] + rist[key][1]
    print(mind)

def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        l = list()
        for k in syote.split(","):
            l.append((k[0], int(k[1:])))
        lista.append(l)
    return lista


if __name__ == "__main__":
    main()
