class IntCode:
    def __init__(self, code):
        self.code = code
        self.save_i = 0

    def execute(self, inp):
        i = self.save_i
        inputi = 0
        output = list()
        suor = 0
        while True:
            # print(self.code, inp)
            ieka = i
            if suor % 1000000 == 0:
                print("Suoritetaan", suor)
            suor += 1
            opc = self.code[i] % 100
            modes = self.code[i] // 100
            try:
                if modes % 10 == 0:
                    v1 = self.code[self.code[i+1]]
                else:
                    v1 = self.code[i+1]
                modes //= 10
            except IndexError:
                v1 = None

            try:
                if modes % 10 == 0:
                    v2 = self.code[self.code[i+2]]
                else:
                    v2 = self.code[i+2]
            except IndexError:
                v2 = None


            modes //= 10
            try:
                if modes % 10 == 0:
                    v3 = self.code[self.code[i+3]]
                else:
                    v3 = self.code[i+3]
            except IndexError:
                v3 = None


            if opc == 99:
                self.save_i = 0
                return output, True
            elif opc == 1:
                self.code[self.code[i+3]] = v1 + v2
                i += 4
            elif opc == 2:
                self.code[self.code[i+3]] = v1 * v2
                i += 4
            elif opc == 3:
                if inputi >= len(inp):
                    print("Vaatii inputtia")
                    self.save_i = i
                    return output, False
                self.code[self.code[i+1]] = inp[inputi]
                inputi += 1
                i += 2
            elif opc == 4:
                output.append(v1)
                i += 2

            elif opc == 5:
                if v1 != 0:
                    i = v2
                else:
                    i += 3

            elif opc == 6:
                if v1 == 0:
                    i = v2
                else:
                    i += 3

            elif opc == 7:
                if v1 < v2:
                    self.code[self.code[i+3]] = 1
                else:
                    self.code[self.code[i + 3]] = 0
                i += 4

            elif opc == 8:
                if v1 == v2:
                    self.code[self.code[i+3]] = 1
                else:
                    self.code[self.code[i + 3]] = 0
                i += 4
            else:
                print("VÄÄRÄ OPC", opc)
                assert False, "VIRHE"
            assert ieka != i, "I EI MUUTTUNUT"

