import numpy as np
from IntCode import IntCode
import itertools

def main():
    lista = get_input()
    print(lista)
    print(test_perm([9, 8, 7, 6, 5], lista))
    input()
    maxt = 0
    for phase in list(itertools.permutations([5, 6, 7, 8, 9])):
        t = test_perm(phase, lista)
        if t > maxt:
            maxp = phase
            maxt = t
    print(maxt, maxp)


def test_perm(phase, lista):
    ics = list()
    inputlist = list()
    for i in range(5):
        ics.append(IntCode(list(lista)))
        inputlist.append([phase[i]])
    inputlist[0].append(0)
    laste = None
    while True:
        for i in range(5):
            print(i, inputlist)
            ic = ics[i]
            outp, halt = ic.execute(inputlist[i])
            inputlist[i] = []
            print(outp, halt)
            if i == 4 and halt:
                return outp[-1]
            inputlist[(i + 1) % 5] += outp



def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        for k in syote.split(","):
            if k == "":
                continue
            lista.append(int(k))
    return lista


if __name__ == "__main__":
    main()
