import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque

def main():
    lista = get_input()
    lista = lista + list(range(10, 1000001))
    print(len(lista))

    jono = dict()
    for i in range(len(lista)-1):
        jono[lista[i]] = lista[i+1]
    jono[lista[-1]] = lista[0]
    print(jono[1])

    lab = lista[0]
    for iter in range(10000000):
        siirra = [jono[lab]]
        for i in range(2):
            siirra.append(jono[siirra[-1]])
        jono[lab] = jono[siirra[-1]]
        dest = lab
        while True:
            dest -= 1
            if dest < 1:
                dest = 1000001
            elif dest in siirra:
                continue
            else:
                old_n = jono[dest]
                jono[dest] = siirra[0]
                jono[siirra[-1]] = old_n
                break
        lab = jono[lab]

        if iter % 100000 == 0:
            print(iter)

    print(jono[1], jono[jono[1]], jono[1] * jono[jono[1]])



def get_input():
    lista = []
    print("anna syöte")
    syote = input()
    for s in syote:
        lista.append(int(s))

    return lista


if __name__ == "__main__":
    main()
