import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque

def main():
    lista = get_input()
    flip = set()
    for s in lista:
        x, y, = 0, 0
        i = 0
        while i < len(s):
            if s[i] not in ('e', 'w'):
                ss = s[i:i+2]
                i += 2
            else:
                ss = s[i]
                i += 1
            if ss == "e":
                x += 1
            elif ss == "w":
                x -= 1
            elif ss == "ne":
                y += 1
            elif ss == "nw":
                y += 1
                x -= 1
            elif ss == "se":
                y -= 1
                x += 1
            elif ss == "sw":
                y -= 1
            else:
                assert False, "V''r' ss: "+ str(ss)
        if (x, y) not in flip:
            flip.add((x, y))
        else:
            flip.remove((x, y))

    neighb = ((-1, 0), (-1, 1), (0, 1), (1, 0), (1, -1), (0, -1))
    for ites in range(100):
        flip2 = set()

        #Mustat
        for x, y in flip:
            t = 0
            for xi, yi in neighb:
                xx, yy = x + xi, y + yi
                if (xx, yy) in flip:
                    t += 1
                    if t > 2:
                        break
            if not(t == 0 or t > 2):
                flip2.add((x, y))

        # Valkoiset
        for x, y in flip:
            for xi, yi in neighb:
                xx, yy = x + xi, y + yi
                t = 0
                for xi2, yi2 in neighb:
                    xx2, yy2 = xx + xi2, yy + yi2
                    if (xx2, yy2) in flip:
                        t += 1
                        if t > 2:
                            break
                if t == 2:
                    flip2.add((xx, yy))
        flip = flip2

        print(ites, len(flip))

def get_input():
    lista = []
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote)

    return lista


if __name__ == "__main__":
    main()
