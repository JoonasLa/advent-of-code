import numpy as np


def main():
    lista = get_input()
    tot = 0
    for num1, num2, let, passw in lista:
        if (passw[num1-1] == let and passw[num2-1] != let) or (passw[num1-1] != let and passw[num2-1] == let):
            tot += 1
    print(tot)

def get_input():
    lista = []
    while True:
        syote = input()
        if syote == "":
            break
        num, let, passw = syote.split(" ")
        num1, num2 = num.split("-")

        lista.append((int(num1), int(num2), let[0], passw))
    return lista


if __name__ == "__main__":
    main()