import numpy as np
from string import ascii_lowercase

def main():
    lista = get_input()
    tot = 0
    print(ascii_lowercase)
    for g in lista:
        kys = set()
        for gg in g:
            for k in ascii_lowercase:
                if k not in gg:
                    kys.add(k)
        tot += len(ascii_lowercase) - len(kys)
    print(tot)



def get_input():
    lista = []
    print("anna syöte")
    d = []
    while True:
        syote = input()
        if syote == ".":
            lista.append(d)
            break
        elif syote == "":
            lista.append(d)
            d = []
        else:
            d.append(syote)
    return lista


if __name__ == "__main__":
    main()