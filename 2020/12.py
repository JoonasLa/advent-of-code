import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase

def main():
    lista = get_input()
    x = 0
    y = 0
    si = 0
    wp = [10, 1]
    sl = [(1, 0), (0, -1), (-1, 0), (0, 1)]
    for suunta, matka in lista:
        if suunta == "N":
            wp[1] += matka
        elif suunta == "S":
            wp[1] -= matka
        elif suunta == "E":
            wp[0] += matka
        elif suunta == "W":
            wp[0] += -matka
        elif suunta == "F":
            x += wp[0] * matka
            y += wp[1] * matka
        elif suunta == "R":
            for _ in range(matka // 90):
                wx = wp[1]
                wy = -wp[0]
                wp = [wx, wy]
        elif suunta == "L":
            for _ in range(matka // 90):
                wx = -wp[1]
                wy = wp[0]
                wp = [wx, wy]
    print(x, y)
    print(abs(x) + abs(y))


def get_input():
    lista = []
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            break
        suunta = syote[0]
        matka = int(syote[1:])
        lista.append((suunta, matka))
    return lista


if __name__ == "__main__":
    main()
