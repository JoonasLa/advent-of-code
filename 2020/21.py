import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque

def main():
    lista = get_input()

    allergeenit = dict()
    for inc, al in lista:
        for a in al:
            if a not in allergeenit:
                allergeenit[a] = set(inc)
            else:
                poista = set()
                for p in allergeenit[a]:
                    if p not in inc:
                        poista.add(p)
                for p in poista:
                    allergeenit[a].remove(p)

    varmat = set()
    v2 = dict()
    while True:
        uvarma = None
        for a in allergeenit:
            if len(allergeenit[a]) == 1:
                uvarma = allergeenit[a].pop()
                varmat.add(uvarma)
                v2[a] = uvarma
                break
        if uvarma is None:
            break
        for a in allergeenit:
            if uvarma in allergeenit[a]:
                allergeenit[a].remove(uvarma)
        print(varmat)
    print(varmat)

    tot = 0
    for inc, _ in lista:
        for i in inc:
            if i not in varmat:
                tot += 1
    print(tot)

    keys = sorted(v2.keys())
    for k in keys:
        print(v2[k], end=",")

def get_input():
    lista = []
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            break
        sanat, sisalto = syote.split(" (contains ")
        s = set(sanat.split(" "))
        s2 = set(sisalto[:-1].split(", "))
        lista.append([s, s2])

    return lista


if __name__ == "__main__":
    main()
