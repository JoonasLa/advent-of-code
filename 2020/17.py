import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque

def main():
    lista = get_input()
    cube = np.zeros((20, 20, 13, 13), dtype=bool)
    for y in range(8):
        for x in range(8):
            if lista[y][x] == "#":
                cube[y+6, x+6, 6, 6] = True


    for i in range(6):
        print(i)
        cube2 = np.zeros((20, 20, 13, 13), dtype=bool)
        for y in range(20):
            for x in range(20):
                for z in range(13):
                    for w in range(13):
                        lkm = laske_lkm(cube, x, y, z, w)
                        if (cube[y, x, z, w] and 2 <= lkm <= 3) or (not cube[y, x, z, w] and lkm == 3):
                            cube2[y, x, z, w] = True
        cube = cube2
    tot = 0
    for y in range(20):
        for x in range(20):
            for z in range(13):
                for w in range(13):
                    if cube[y, x, z, w]:
                        tot += 1
    print(tot)

@njit()
def laske_lkm(cube, x, y, z, w):
    lukm = 0
    for yy in range(max(0, y - 1), min(20, y + 2)):
        for xx in range(max(0, x - 1), min(20, x + 2)):
            for zz in range(max(0, z - 1), min(13, z + 2)):
                for ww in range(max(0, w- 1), min(13, w + 2)):
                    if yy == y and xx == x and zz == z and ww == w:
                        continue
                    if cube[yy, xx, zz, ww]:
                        lukm += 1
                        if lukm > 3:
                            return lukm
    return lukm


def get_input():
    lista = []
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote)
    return lista


if __name__ == "__main__":
    main()
