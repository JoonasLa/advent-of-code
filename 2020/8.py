import numpy as np

def main():
    syote = get_input()
    acc = 0
    i_kayty = np.zeros(len(syote), dtype=bool)
    i = 0
    while not i_kayty[i]:
        i_kayty[i] = True
        com, num = syote[i].split(" ")
        if com == "acc":
            acc += int(num)
            i += 1
        elif com == "jmp":
            i += int(num)
        elif com == "nop":
            i += 1
        else:
            assert False, "virhe"
    print(acc)

def main2():
    syote = get_input()
    i_change = -1
    while True:
        acc = 0
        i_kayty = np.zeros(len(syote), dtype=bool)
        while True:
            i_change += 1
            com, num = syote[i_change].split(" ")
            if com == "jmp":
                syote[i_change] = "nop " + num
                break
            elif com == "nop":
                syote[i_change] = "jmp " + num
                break
        print(i_change, "testaan")
        i = 0
        while i < len(i_kayty) and not i_kayty[i]:
            i_kayty[i] = True
            com, num = syote[i].split(" ")
            if com == "acc":
                acc += int(num)
                i += 1
            elif com == "jmp":
                i += int(num)
            elif com == "nop":
                i += 1
            else:
                assert False, "virhe"
        if i == len(i_kayty):
            print(acc)
            break

        com, num = syote[i_change].split(" ")
        if com == "jmp":
            syote[i_change] = "nop " + num
        elif com == "nop":
            syote[i_change] = "jmp " + num

def get_input():
    rivit = []
    while True:
        syote = input()
        if syote == "":
            break
        rivit.append(syote)
    return rivit


if __name__ == "__main__":
    main2()