import numpy as np


def main():
    lista = get_input()
    maxtulos = 0
    loytyy = np.zeros(128*8, dtype=bool)
    for c in lista:
        mini = 0
        maxi = 127
        kerroin = 64
        for i in range(7):
            if c[i] == "B":
                mini += kerroin
            elif c[i] == "F":
                maxi -= kerroin
            kerroin = kerroin // 2

        y = mini
        mini = 0
        maxi = 7
        kerroin = 4
        for i in range(7, 10):
            if c[i] == "R":
                mini += kerroin
            elif c[i] == "L":
                maxi -= kerroin
            kerroin = kerroin // 2
        x = mini
        tulos = y*8 + x
        loytyy[tulos] = True
    print(np.argwhere(~loytyy))

def get_input():
    lista = []
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            break

        lista.append(syote)
    return lista


if __name__ == "__main__":
    main()