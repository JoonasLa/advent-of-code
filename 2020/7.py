
def main():
    # solve_1(bags)
    solve_2()


def solve_1(bags):
    bags = read_bags()
    has_target = dict()
    for b in bags:
        print(b, "sisältää", bags[b])
        has_target[b] = False

    has_target["shiny gold"] = True
    while True:
        new_added = False
        for b in bags:
            if has_target[b]:
                continue
            for b2 in bags[b]:
                if has_target[b2]:
                    new_added = True
                    has_target[b] = True
                    break
        if not new_added:
            break
    summa = 0
    for b in has_target:
        if has_target[b] and b != "shiny gold":
            print(b, "sisältää")
            summa += 1

    print("Yhteensä", summa)


def read_bags():
    rivit = []
    print("Syötä input")
    while True:
        syote = input()
        if syote == "":
            break
        rivit.append(syote)

    contains = dict()
    for rivi in rivit:
        sanat = rivi.split(" ")
        laukku1 = sanat[0] + " " + sanat[1]
        contains[laukku1] = set()
        if len(sanat) % 4 != 0:
            continue
        for i in range(5, len(sanat), 4):
            laukku2 = sanat[i] + " " + sanat[i+1]
            contains[laukku1].add(laukku2)
    assert len(rivit) == len(contains), "mÄÄRÄT EI TÄSMÄÄ"
    return contains


def solve_2():
    bags = read_bags2()
    num_bags = dict()
    for b in bags:
        num_bags[b] = None
    while True:
        for b in bags:
            summa = 1
            for kerroin, b2 in bags[b]:
                if num_bags[b2] is None:
                    break
                summa += kerroin * num_bags[b2]
            else:
                num_bags[b] = summa
                print(b, summa)
                if b == "shiny gold":
                    print("Vastaus", summa - 1)
                    return

def read_bags2():
    rivit = []
    print("Syötä input")
    while True:
        syote = input()
        if syote == "":
            break
        rivit.append(syote)

    contains = dict()
    for rivi in rivit:
        sanat = rivi.split(" ")
        laukku1 = sanat[0] + " " + sanat[1]
        contains[laukku1] = set()
        if len(sanat) % 4 != 0:
            continue
        for i in range(4, len(sanat), 4):
            maara = int(sanat[i])
            laukku2 = sanat[i + 1] + " " + sanat[i + 2]
            contains[laukku1].add((maara, laukku2))
    assert len(rivit) == len(contains), "mÄÄRÄT EI TÄSMÄÄ"
    return contains


if __name__ == "__main__":
    main()
