import numpy as np
from pyperclip import copy

def main():
    lista = get_input()
    tot = 0
    vals = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

    for d in lista:
        to = 0
        for kay in d:
            if kay in vals:
                to += 1
        if to == 7:
            if not 1920<=int(d["byr"])<=2002:
                continue

            if not 2010<=int(d["iyr"]) <= 2020:
                continue

            if not 2020 <= int(d["eyr"]) <= 2030:
                continue

            if not ((d["hgt"][-2:] == "cm" and 150 <= int(d["hgt"][:-2]) <= 193) or (d["hgt"][-2:] == "in" and 59 <= int(d["hgt"][:-2]) <= 76)):
                continue
            t5 = True
            if d["hcl"][0] == "#" and len(d["hcl"]) == 7:
                for i in range(6):
                    if i+1 >= len(d["hcl"]):
                        t5 = False
                        break
                    if d["hcl"][i+1] not in ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"):
                        t5 = False
                        break
            else:
                continue
            if not t5:
                continue

            t6 = True
            if len(d["pid"]) != 9:
                continue
            for t in d["pid"]:
                if t not in ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"):
                    t6 = False
                    break
            if not t6:
                continue

            if d["ecl"] not in ("amb", "blu", "brn", "gry", "grn", "hzl", "oth"):
                continue
            tot += 1

    print(tot)


def get_input():
    lista = []
    print("anna syöte")
    d = dict()
    while True:
        syote = input()
        if syote == "x":
            lista.append(d)
            break
        elif syote == "":
            lista.append(d)
            d = dict()
        else:
            sanat = syote.split(" ")
            for s in sanat:
                key, val = s.split(":")
                d[key] = val

    return lista


if __name__ == "__main__":
    main()