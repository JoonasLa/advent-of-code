import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque


def main():
    rules, sanat = get_input()
    already_calc = dict()
    already_calc[92] = ["a"]
    already_calc[13] = ["b"]
    for i in range(100):
        print(i, rules[i])
    all_pos = get_all_possible(rules, 0, already_calc)
    print(len(all_pos))

    tot = 0
    all_pos = set(all_pos)
    eka = set(already_calc[42])
    toka = set(already_calc[31])
    for s in eka:
        if len(s) != 8:
            print("BIRHE")
    for s in toka:
        if len(s) != 8:
            print("BOIFE")
    print(eka)
    print(toka)

    for s in sanat:
        skop = s
        if len(s) % 8 != 0:
            continue
        ekanum = 0
        while len(s) > 0:
            if s[:8] in eka:
                ekanum += 1
                s = s[8:]
            else:
                break

        tokanum = 0
        while len(s) > 0:
            if s[:8] in toka:
                tokanum += 1
                s = s[8:]
            else:
                break

        if 0 < tokanum < ekanum and len(s) == 0:
            print(skop)
            tot += 1

    print(tot)

def get_all_possible(rules, rulenum, already_calc):
    lista = []
    for rulelist in rules[rulenum]:
        pos2 = []
        for num in rulelist:
            if num not in already_calc:
                res = get_all_possible(rules, num, already_calc)
                already_calc[num] = res
            pos2.append(already_calc[num])
        if len(pos2) == 1:
            for i in range(len(pos2[0])):
                lista.append(pos2[0][i])
        elif len(pos2) == 2:
            for i in range(len(pos2[0])):
                for j in range(len(pos2[1])):
                    lista.append(pos2[0][i] + pos2[1][j])
    return lista

def get_input():
    rules = dict()
    sanat = []
    print("anna syöte")
    case = 0
    while True:
        syote = input()
        if syote == "":
            case += 1
            if case == 2:
                break
        if case == 0:
            key, rul = syote.split(": ")
            if rul == '"a"':
                rules[int(key)] = "a"
                continue
            elif rul == '"b"':
                rules[int(key)] = "b"
                continue
            rul2 = rul.split(" | ")
            l = []
            for r in rul2:
                ss = r.split(" ")
                l2 = []
                for sss in ss:
                    if sss != "":
                        l2.append(int(sss))
                l.append(l2)
            rules[int(key)] = l
        elif case == 1:
            sanat.append(syote)

    return rules, sanat


if __name__ == "__main__":
    main()
