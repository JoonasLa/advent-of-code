import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque

def main():
    valid, oma, muut = get_input()
    muut2 = []

    sopivat = np.zeros(1000, dtype=bool)
    for key in valid:
        for alku, loppu in valid[key]:
            sopivat[alku:loppu+1] = True

    tot = 0
    for l in muut:
        sopii = True
        for ll in l:
            if not sopivat[ll]:
                sopii = False
                tot += ll
        if sopii:
            muut2.append(l)

    muut = muut2
    sopivat = dict()
    for i in range(20):
        sopivat[i] = set(valid.keys())
    for l in muut:
        for i in range(20):
            poista = set()
            for key in sopivat[i]:
                loytyi = False
                for alku, loppu in valid[key]:
                    if len(l) != 20:
                        print(l)
                    if alku <= l[i] <= loppu:
                        loytyi = True
                        break
                if not loytyi:
                    poista.add(key)
            for key in poista:
                sopivat[i].remove(key)
    tot = 1
    print(sopivat)
    varmat = dict()
    while len(varmat) < 20:
        for i in range(20):
            if len(sopivat[i]) == 1:
                poista = sopivat[i].pop()
                varmat[i] = poista
                break
        for i in range(20):
            if poista in sopivat[i]:
                sopivat[i].remove(poista)
    for i in range(20):
        if varmat[i][:9] == "departure":
            tot *= oma[i]
    print(tot)




def get_input():
    valid = dict()
    oma = []
    muut = []
    print("anna syöte")
    case = 0
    while True:
        syote = input()
        if syote == "":
            case += 1
            if case == 3:
                break
            continue
        if case == 0:
            key, arvo = syote.split(": ")
            parit = arvo.split(" or ")
            l = []
            for i in range(len(parit)):
                alku, loppu = parit[i].split("-")
                l.append((int(alku), int(loppu)))
            valid[key] = l
        elif case == 1:
            if syote != "your ticket:":
                arvot = syote.split(",")
                for a in arvot:
                    oma.append(int(a))
        elif case == 2:
            if syote != "nearby tickets:":
                arvot = syote.split(",")
                a = []
                for aa in arvot:
                    a.append(int(aa))
                muut.append(a)
    return valid, oma, muut


if __name__ == "__main__":
    main()
