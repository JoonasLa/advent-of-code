import numpy as np
from pyperclip import copy
from numba import njit

def main():
    lista = get_input()
    lista.append(0)
    lista.append(max(lista) + 3)
    lista = sorted(lista)
    lista = np.array(lista)
    global lasketut
    lasketut = dict()
    print(recursive(lista, 0, 1))


def recursive(lista, ed, i):
    if i == len(lista)-1:
        return 1
    global lasketut

    voi_poistaa = lista[i+1] - ed < 4
    if voi_poistaa:
        t1 = (lista[i], i+1)
        t2 = (ed, i+1)
        if t1 not in lasketut:
            lasketut[t1] = recursive(lista, lista[i], i+1)
        if t2 not in lasketut:
            lasketut[t2] = recursive(lista, ed, i+1)
        return lasketut[t1] + lasketut[t2]
    else:
        t1 = (lista[i], i+1)
        if t1 not in lasketut:
            lasketut[t1] = recursive(lista, lista[i], i+1)
        return lasketut[t1]


def get_input():
    lista = []
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            break

        lista.append(int(syote))
    return lista


if __name__ == "__main__":
    main()