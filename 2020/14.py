import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque

def main():
    lista = get_input()
    mem = dict()
    mask = None
    for m, num in lista:
        if m == "m":
            mask = num
        else:
            memd2 = []
            for i in range(36):
                memd2.append(m % 2)
                m = m // 2
            memd2 = memd2[::-1]
            for i in range(36):
                if mask[i] == "1":
                    memd2[i] = 1
                elif mask[i] == "X":
                    memd2[i] = "X"
            dq = deque()
            dq.append(memd2)
            while True:
                f = dq.popleft()
                for i in range(36):
                    if f[i] == "X":
                        m2 = list(f)
                        m2[i] = 0
                        dq.append(m2)
                        m2 = list(f)
                        m2[i] = 1
                        dq.append(m2)
                        break
                else:
                    dq.append(f)
                    break
            for memd2 in dq:
                luku = 0
                for i in range(36):
                    luku += 2 ** (35-i) * memd2[i]
                mem[luku] = num

    tot = 0
    for m in mem:
        tot += mem[m]
        print(m, mem[m])
    print(tot)

def get_input():
    lista = []
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            break
        m, num = syote.split(" = ")
        if m == "mask":
            lista.append(('m', num))
        else:
            lista.append((int(m[4:-1]), int(num)))
    return lista


if __name__ == "__main__":
    main()
