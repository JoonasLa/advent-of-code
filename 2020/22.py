import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque

def main():
    p1, p2 = get_input()
    p1 = deque(p1)
    p2 = deque(p2)

    _, voittaja = rec_game(p1, p2)

    print(voittaja)
    tot = 0
    for i in range(len(voittaja)):
        tot += voittaja[-(i+1)] * (i + 1)
    print(tot)

def rec_game(p1, p2):
    pelatut = set()
    while True:
        t = (tuple(p1), tuple(p2))
        if t in pelatut:
            return 1, p1
        pelatut.add(t)
        c1 = p1.popleft()
        c2 = p2.popleft()
        if c1 <= len(p1) and c2 <= len(p2):
            pp1 = deque()
            pp2 = deque()
            for i in range(c1):
                pp1.append(p1[i])
            for i in range(c2):
                pp2.append(p2[i])
            winner, _ = rec_game(pp1, pp2)
        elif c1 > c2:
            winner = 1
        else:
            winner = 2

        if winner == 1:
            p1.append(c1)
            p1.append(c2)
            if len(p2) == 0:
                return 1, p1
        else:
            p2.append(c2)
            p2.append(c1)
            if len(p1) == 0:
                return 2, p2

def get_input():
    p1 = []
    p2 = []
    case = 0
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            case += 1
            if case == 2:
                break
        elif case == 0 and len(syote) < 3:
            p1.append(int(syote))
        elif case == 1 and len(syote) < 3:
            p2.append(int(syote))
    return p1, p2


if __name__ == "__main__":
    main()
