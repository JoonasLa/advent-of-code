import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase

def main():
    lista = get_input()

    while True:
        muuttui = False
        lista2 = []
        for y in range(len(lista)):
            rivi2 = []
            for x in range(len(lista[0])):
                tyhjia = 0
                varattuja = 0
                for xi, yi in ((1, 0), (-1, 0), (0, 1), (0, -1), (-1, -1), (-1, 1), (1, -1), (1, 1)):
                    for i in range(1, max(len(lista), len(lista[0]))):
                        xx = x + xi * i
                        yy = y + yi * i

                        if (not 0 <= xx < len(lista[0])) or (not 0 <= yy < len(lista)):
                            tyhjia += 1
                            break
                        if lista[yy][xx] == "L":
                            tyhjia += 1
                            break
                        if lista[yy][xx] == "#":
                            varattuja += 1
                            break
                if lista[y][x] == "L":
                    if varattuja == 0:
                        rivi2.append("#")
                        muuttui = True
                    else:
                        rivi2.append("L")
                elif lista[y][x] == "#":
                    if varattuja >= 5:
                        rivi2.append("L")
                        muuttui = True
                    else:
                        rivi2.append("#")
                else:
                    rivi2.append(".")
            lista2.append(rivi2)
        lista = lista2
        if not muuttui:
            break
        for y in range(len(lista)):
            for x in range(len(lista[0])):
                print(lista[y][x], end="")
            print()

        print()

    tot = 0
    for y in range(len(lista)):
        for x in range(len(lista[0])):
            if lista[y][x] == "#":
                tot += 1
    print(tot)


def get_input():
    lista = []
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            break
        rivi = []
        for k in syote:
            rivi.append(k)
        lista.append(rivi)
    return lista


if __name__ == "__main__":
    main()
