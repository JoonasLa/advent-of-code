import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque

def main():
    dpk = 17607508
    cpk = 15065270

    dl = 1
    val = 1
    while True:
        val = (val * 7) % 20201227
        if val == dpk:
            break
        dl += 1
    print(dl)

    cl = 1
    val = 1
    while True:
        val = (val * 7) % 20201227
        if val == cpk:
            break
        cl += 1
    print(cl)

    val = 1
    for _ in range(dl):
        val = (val * cpk) % 20201227
    print(val)

def get_input():
    lista = []
    print("anna syöte")
    syote = input()
    for s in syote:
        lista.append(syote)

    return lista


if __name__ == "__main__":
    main()
