import numpy as np


def main():
    lista = get_input()

    toto = 1
    for xx, yy in ((1, 1), (3, 1), (5, 1), (7, 1), (1, 2)):
        tot = 0
        x = 0
        y = 0
        while True:
            x = (x + xx) % len(lista[0])
            y += yy
            if y >= len(lista):
                break
            if lista[y][x] == "#":
                tot += 1
        toto *= tot
    print(toto)

def get_input():
    lista = []
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            break

        lista.append(syote)
    return lista


if __name__ == "__main__":
    main()