import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque

def main():
    lista = get_input()
    last_ind = dict()
    for i in range(len(lista)-1):
        last_ind[lista[i]] = i
    for i in range(len(lista), 30000000):
        if lista[i-1] in last_ind:
            lista.append(i-1-last_ind[lista[i-1]])
        else:
            lista.append(0)
        last_ind[lista[i-1]] = i - 1
    # print(lista)
    print(lista[-1])


def get_input():
    lista = [2,0,1,7,4,14,18]
    return lista


if __name__ == "__main__":
    main()
