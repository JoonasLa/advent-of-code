import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque


def main():
    lista = get_input()
    print(len(lista))
    reunat = dict()


    kuva = lista[3457][1:9, 1:9]
    print(3457, kuva)
    kuva2 = kierra_kuva(kuva, 5)
    print("\n", kuva2)
    input()

    for key in lista:
        l = []
        l.append(lista[key][0, :])
        l.append(lista[key][:, -1])
        l.append(lista[key][-1, ::-1])
        l.append(lista[key][::-1, 0])
        reunat[key] = l
    keys = list(reunat.keys())
    sopii_vaaka = set()
    sopii_pysty = set()
    valm_lasketut = dict()
    for k in keys:
        for rot in range(16):
            valm_lasketut[(k, rot)] = kierra(reunat[k], rot)
    print("Valm_lasketut", len(valm_lasketut))
    for k1 in keys:
        print(k1)
        for k2 in keys:
            if k1 == k2:
                continue
            for rot1 in range(16):
                for rot2 in range(16):
                    p1 = valm_lasketut[(k1, rot1)]
                    p2 = valm_lasketut[(k2, rot2)]
                    if sopii(p1[2][::-1], p2[0]):
                        sopii_pysty.add((k1, rot1, k2, rot2))
                    if sopii(p1[1], p2[3][::-1]):
                        sopii_vaaka.add((k1, rot1, k2, rot2))
    print("Laskettu", len(sopii_pysty), len(sopii_vaaka))
    print(sopii_pysty)
    print(sopii_vaaka)
    input()
    keskipalat = set()
    reunapalat = set()
    kulmapalat = set()
    for k in keys:
        tot = 0
        for pys in sopii_pysty:
            if pys[:2] == (k, 0):
                tot += 1
                break
        for pys in sopii_pysty:
            if pys[2:] == (k, 0):
                tot += 1
                break
        for pys in sopii_vaaka:
            if pys[:2] == (k, 0):
                tot += 1
                break
        for pys in sopii_vaaka:
            if pys[2:] == (k, 0):
                tot += 1
                break
        if tot == 4:
            keskipalat.add(k)
        elif tot == 3:
            reunapalat.add(k)
        else:
            kulmapalat.add(k)
        print(k, tot)
    input() # 1613 1321 1093 3217


    vast = np.zeros((12, 12), dtype=int)
    vast[0, 0] = 1093
    kierrot = np.zeros((12, 12), dtype=int)
    kulmapalat.remove(1093)
    sop = ratkaise(1, vast, kierrot, sopii_pysty, sopii_vaaka, keskipalat, reunapalat, kulmapalat)
    print("Valmis", sop)
    for y in range(12):
        for x in range(12):
            print(vast[y, x], kierrot[y, x])

    kuva = np.zeros((8*12, 8*12), dtype=bool)
    for y in range(12):
        for x in range(12):
            k = lista[vast[y, x]][1:9, 1:9]
            rot = kierrot[y, x]
            k2 = kierra_kuva(k, rot)
            kuva[y*8:y*8+8, x*8:x*8+8] = k2
    np.save("kuva", kuva)
    print(kuva)



@njit()
def ratkaise(i, vast, kierrot, sopii_pysty, sopii_vaaka, keskipalat, reunapalat, kulmapalat):
    print(i)
    if i == 144:
        return True
    x, y = i % 12, i // 12
    kulma = (x == 0 or x == 11) and (y == 0 or y == 11)
    reuna = x == 0 or x == 11 or y == 0 or y == 11
    if kulma:
        for k in kulmapalat:
            for rot in range(16):
                if x > 0:
                    if (vast[y, x-1], kierrot[y, x-1], k, rot) not in sopii_vaaka:
                        continue
                if y > 0:
                    if (vast[y-1, x], kierrot[y-1, x], k, rot) not in sopii_pysty:
                        continue
                vast[y, x] = k
                kierrot[y, x] = rot
                kulma2 = set(kulmapalat)
                kulma2.remove(k)
                sop = ratkaise(i+1, vast, kierrot, sopii_pysty, sopii_vaaka, keskipalat, reunapalat, kulma2)
                if sop:
                    return True
    elif reuna:
        for k in reunapalat:
            for rot in range(16):
                if x > 0:
                    if (vast[y, x - 1], kierrot[y, x - 1], k, rot) not in sopii_vaaka:
                        continue
                if y > 0:
                    if (vast[y - 1, x], kierrot[y - 1, x], k, rot) not in sopii_pysty:
                        continue
                vast[y, x] = k
                kierrot[y, x] = rot
                reunat2 = set(reunapalat)
                reunat2.remove(k)
                sop = ratkaise(i + 1, vast, kierrot, sopii_pysty, sopii_vaaka, keskipalat, reunat2, kulmapalat)
                if sop:
                    return True
    else:
        for k in keskipalat:
            for rot in range(16):
                if x > 0:
                    if (vast[y, x-1], kierrot[y, x-1], k, rot) not in sopii_vaaka:
                        continue
                if y > 0:
                    if (vast[y-1, x], kierrot[y-1, x], k, rot) not in sopii_pysty:
                        continue
                vast[y, x] = k
                kierrot[y, x] = rot
                keski2 = set(keskipalat)
                keski2.remove(k)
                sop = ratkaise(i+1, vast, kierrot, sopii_pysty, sopii_vaaka, keski2, reunapalat, kulmapalat)
                if sop:
                    return True
    return False


def sopii(ar1, ar2):
    return np.all(ar1 == ar2)


def kierra(palikka, kierto):
    p2 = []
    for p in palikka:
        p2.append(np.array(p))

    if kierto // 8 == 1:
        p2[0], p2[2] = p2[2][::-1], p2[0][::-1]
        p2[1], p2[3] = p2[1][::-1], p2[3][::-1]
        kierto -= 8

    if kierto // 4 == 1:
        p2[1], p2[3] = p2[3][::-1], p2[1][::-1]
        p2[0], p2[2] = p2[0][::-1], p2[2][::-1]
        kierto -= 4
    assert 0 <= kierto < 4, "huono kierto"

    p3 = []
    for i in range(4):
        p3.append(p2[(i - kierto) % 4])

    return p3

def kierra_kuva(palikka, rot):
    if rot // 8 == 1:
        palikka = palikka[::-1, :]
        rot -= 8
    if rot // 4 == 1:
        palikka = palikka[:, ::-1]
        rot -= 4
    p2 = np.zeros((8, 8), dtype=bool)
    assert 0 <= rot < 4, "väärä rot"
    if rot == 1:
        for y in range(8):
            p2[y, :] = palikka[::-1, y]
    elif rot == 2:
        for y in range(8):
            p2[y, :] = palikka[7-y, ::-1]
    elif rot == 3:
        for y in range(8):
            p2[y, :] = palikka[:, 7 - y]
    else:
        p2 = np.array(palikka)
    return p2


def get_input():
    lista = dict()
    print("anna syöte")
    tiili = np.zeros((10, 10), dtype=bool)
    tilenum = 0
    rivinum = 0
    while True:
        syote = input()
        if syote == "":
            if rivinum == 0:
                break
            lista[tilenum] = tiili
            tiili = np.zeros((10, 10), dtype=bool)
            rivinum = 0
        elif syote[0] == "T":
            tilenum = int(syote[5:9])
        else:
            for i in range(len(syote)):
                if syote[i] == "#":
                    tiili[rivinum, i] = True
            rivinum += 1

    return lista

def etsi_monsterit(kuva):
    monsteri = []
    monsteri.append("                  # ")
    monsteri.append("#    ##    ##    ###")
    monsteri.append(" #  #  #  #  #  #   ")
    m = np.ones((3, len(monsteri[0])), dtype=bool)
    for y in range(3):
        for x in range(len(monsteri[0])):
            if monsteri[y][x] == "#":
                m[y, x] = False
    tot = 0
    for y in range(8*12-3):
        for x in range(8*12 - m.shape[1]):
            if np.all(np.logical_or(kuva[y:y+3, x:x+m.shape[1]], m)):
                tot += 1

    return tot

def kierra_kuva2(palikka, rot):
    if rot // 8 == 1:
        palikka = palikka[::-1, :]
        rot -= 8
    if rot // 4 == 1:
        palikka = palikka[:, ::-1]
        rot -= 4
    p2 = np.zeros((8*12, 8*12), dtype=bool)
    assert 0 <= rot < 4, "väärä rot"
    if rot == 1:
        for y in range(8*12):
            p2[y, :] = palikka[::-1, y]
    elif rot == 2:
        for y in range(8*12):
            p2[y, :] = palikka[8*12-1-y, ::-1]
    elif rot == 3:
        for y in range(8*12):
            p2[y, :] = palikka[:, 8*12-1 - y]
    else:
        p2 = np.array(palikka)
    return p2


def main2():
    kuva = np.load("kuva.npy")
    for rot in range(8):
        k2 = kierra_kuva2(kuva, rot)
        tot = etsi_monsterit(k2)
        print(rot, tot)
        if tot != 0:
            t = np.sum(kuva[:]) - tot * 15
            print(t)
            break
if __name__ == "__main__":
    main2()
