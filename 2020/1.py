import numpy as np


def main():
    lista = get_input()
    for i in range(len(lista)):
        for j in range(i+1, len(lista)):
            for k in range(j+1, len(lista)):
                if lista[i] + lista[j] + lista[k] == 2020:
                    print(lista[i]*lista[j]*lista[k])
                    return

def get_input():
    lista = []
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(int(syote))
    return lista


if __name__ == "__main__":
    main()