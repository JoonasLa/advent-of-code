import numpy as np
from pyperclip import copy
from numba import njit
from string import ascii_lowercase
from collections import deque

def main():
    lista = get_input()
    tot = 0

    for ll in lista:
        luku = laske(ll)
        print(luku)
        tot += luku
    print(tot)


def laske(lause):
    print("lause", lause)
    lause2 = []
    i = 0
    while i < len(lause):
        if lause[i] == "(":
            sulkuja = 1
            for ii in range(i+1, len(lause)):
                if lause[ii] == "(":
                    sulkuja += 1
                elif lause[ii] == ")":
                    sulkuja -= 1
                    if sulkuja == 0:
                        break
            lause2.append(laske(lause[i+1:ii]))
            i = ii
        else:
            lause2.append(lause[i])
        i += 1
    lause3 = []
    i = 0
    print("lause2", lause2)
    while i < len(lause2):
        if lause2[i] == "+":
            edel = lause3.pop()
            lause3.append(edel + lause2[i+1])
            i += 1
        else:
            lause3.append(lause2[i])
        i += 1
    print("lause3", lause3)
    tulos = 1
    for i in range(len(lause3)):
        if lause3[i] != "*":
            tulos *= lause3[i]
    return tulos

def get_input():
    lista = []
    print("anna syöte")
    while True:
        syote = input()
        if syote == "":
            break
        l = []
        for s in syote:
            if s in ("+", "*", "(", ")"):
                l.append(s)
            elif s == " ":
                continue
            else:
                l.append(int(s))
        lista.append(l)
    return lista


if __name__ == "__main__":
    main()
