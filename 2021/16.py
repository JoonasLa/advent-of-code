import numpy as np


def main():
    lista = get_input()
    print(lista)
    num, _ = read_new(lista)
    print(num)


def read_new(bits):
    id = bit2num(bits[3:6])
    print(f"{id=}")
    rem = bits[6:]
    totbiti = 6
    if id == 4:
        num, biti = read_literal(rem)
        return num, biti + totbiti
    else:
        nums = list()
        totbiti += 1
        if rem[0] == 0:
            totbiti += 15
            bitlen = bit2num(rem[1:16])
            print(f"{bitlen=}")
            totbiti += bitlen
            rem2 = rem[16:16 + bitlen]
            while len(rem2) > 0:
                print(f"{rem2=}")
                num, biti = read_new(rem2)
                rem2 = rem2[biti:]
                nums.append(num)

        elif rem[0] == 1:
            totbiti += 11
            bitnum = bit2num(rem[1:12])
            print(f"{bitnum=}")
            rem2 = rem[12:]
            for _ in range(bitnum):
                num, biti = read_new(rem2)
                totbiti += biti
                nums.append(num)
                rem2 = rem2[biti:]
        print(f"{id=} {nums=}")
        if id == 0:
            return sum(nums), totbiti
        elif id == 1:
            p = 1
            for n in nums:
                p *= n
            return p, totbiti
        elif id == 2:
            return min(nums), totbiti
        elif id == 3:
            return max(nums), totbiti
        elif id == 5:
            if nums[0] > nums[1]:
                return 1, totbiti
            else:
                return 0, totbiti
        elif id == 6:
            if nums[0] < nums[1]:
                return 1, totbiti
            else:
                return 0, totbiti
        elif id == 7:
            if nums[0] == nums[1]:
                return 1, totbiti
            else:
                return 0, totbiti


def read_literal(bits):
    b2 = list()
    i = 0
    while bits[i] == 1:
        b2 += bits[i+1:i+5]
        i += 5
    b2 += bits[i+1:i+5]
    return bit2num(b2), i+5



def bit2num(bits):
    i = 1
    tot = 0
    for k in bits[::-1]:
        tot += k * i
        i *= 2
    return tot


def get_input():
    d = {"0":"0000", "1":"0001", "2":"0010", "3":"0011", "4":"0100", "5":"0101", "6":"0110", "7":"0111", "8":"1000", "9":"1001", "A":"1010", "B":"1011", "C":"1100", "D":"1101", "E":"1110", "F":"1111"}
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        for k in syote:
            for kk in d[k]:
                lista.append(int(kk))
    return lista


if __name__ == "__main__":
    main()
