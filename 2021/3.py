import numpy as np


def main():
    lista = get_input()
    num = []
    for k in range(len(lista[0])):
        n1 = 0
        for kk in lista:
            if kk[k] == "1":
                n1 += 1
        if n1 > len(lista)//2:
            num.append(1)
        else:
            num.append(0)
    print(num)
    t1 = 0
    t2 = 0
    for k in range(12):
        t1 += 2**(11-k)*num[k]
        t2 += 2**(11-k) * (1-num[k])
    print(t1*t2)

    lista2 = list(lista)
    k = 0
    while True:
        l1 = list()
        l2 = list()
        for i in range(len(lista2)):
            if lista2[i][k] == "1":
                l1.append(lista2[i])
            else:
                l2.append(lista2[i])
        if len(l1) >= len(l2):
            lista2 = l1
        else:
            lista2 = l2
        k += 1
        if len(lista2) < 2:
            print(lista2)
            break
    t1 = lista2[0]

    lista2 = list(lista)
    k = 0
    while True:
        l1 = list()
        l2 = list()
        for i in range(len(lista2)):
            if lista2[i][k] == "1":
                l1.append(lista2[i])
            else:
                l2.append(lista2[i])
        if len(l1) < len(l2):
            lista2 = l1
        else:
            lista2 = l2
        k += 1
        if len(lista2) < 2:
            print(lista2)
            break
    t2 = lista2[0]
    print(t1, t2)
    l1 = 0
    l2 = 0
    for k in range(12):
        l1 += 2 ** (11 - k) * int(t1[k])
        l2 += 2 ** (11 - k) * int(t2[k])
    print(l1 * l2)


def get_input():
    lista = []
    print("Anna syöte:")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote)
    return lista


if __name__ == "__main__":
    main()
