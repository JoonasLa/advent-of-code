import numpy as np


def main():
    lista = get_input()
    print(lista)

    step = 1
    while True:
        edmove = list()
        for l in lista:
            edmove.append(list(l))
        move(lista)
        # print(lista)
        if lista == edmove:
            break
        step += 1
    print(step)

def move(lista):

    w = len(lista[0])
    h = len(lista)

    moves = set()
    for y in range(h):
        for x in range(w):
            if lista[y][x] == ">" and lista[y][(x+1) % w] == ".":
                moves.add((x, y))
    for x, y in moves:
        lista[y][x] = "."
        lista[y][(x+1)%w] = ">"

    moves = set()
    for y in range(h):
        for x in range(w):
            if lista[y][x] == "v" and lista[(y+1)%h][x] == ".":
                moves.add((x, y))
    for x, y in moves:
        lista[y][x] = "."
        lista[(y+1)%h][x] = "v"



def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        l = list()
        for k in syote:
            l.append(k)
        lista.append(l)
    return lista


if __name__ == "__main__":
    main()
