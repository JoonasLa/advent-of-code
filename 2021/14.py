import numpy as np


def main():
    alku, lista = get_input()

    parit = dict()
    for i in range(len(alku)-1):
        kood = alku[i:i+2]
        if kood not in parit:
            parit[kood] = 0
        parit[kood] += 1
    print(parit)
    print(len(lista))
    for gen in range(40):
        parit2 = dict()
        for key in parit:
            s = parit[key]
            if key in lista:
                k1 = key[0] + lista[key]
                if k1 not in parit2:
                    parit2[k1] = 0
                parit2[k1] += s
                k2 = lista[key] + key[1]
                if k2 not in parit2:
                    parit2[k2] = 0
                parit2[k2] += s
            else:
                if key not in parit2:
                    parit2[key] = 0
                parit2[key] += parit[key]
        parit = parit2
        # print(gen, parit)
    numlet = dict()
    for key in parit:
        s = key[0]
        if s not in numlet:
            numlet[s] = 0
        numlet[s] += parit[key]


    l2 = sorted(list(numlet.keys()), key= lambda x: numlet[x])
    for key in l2[::-1]:
        print(f"{key:2s} {numlet[key]:10d}")
    tulos = numlet[l2[-1]] - numlet[l2[0]]
    if alku[-1] == l2[-1]:
        tulos += 1
    if alku[-1] == l2[0]:
        tulos -= 1

    print(tulos)


def get_input():
    lista = dict()
    alku = ""
    print("Anna syöte:", end="")
    tila = 0
    while True:
        syote = input()
        if syote == "":
            tila += 1
            if tila == 2:
                break
            continue
        if tila == 0:
            alku = syote
        else:
            s1, s2 = syote.split(" -> ")
            assert s1 not in lista, "virhe"
            lista[s1] = s2

    return alku, lista


if __name__ == "__main__":
    main()
