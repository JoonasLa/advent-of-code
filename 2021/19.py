import numpy as np


def main():
    lista = get_input()
    print(len(lista), lista[0])

    abs_coord = set()
    abs_coord.add(0)
    not_abs_coord = set()
    for i in range(1, len(lista)):
        not_abs_coord.add(i)

    abspos = [(0, 0, 0)] + [None] * (len(lista)-1)
    while not_abs_coord:
        valmis = None
        print(abs_coord, not_abs_coord)
        for i in abs_coord:
            for j in not_abs_coord:
                paal, a1, b1 = paallekkain(lista[i], lista[j])
                if paal:
                    print(i, j, a1, b1)
                    ul, relpos = sijainti(lista[i], lista[j], a1, b1)
                    abspos[j] = relpos
                    lista[j] = ul
                    valmis = (i, j)
                    break
            if valmis:
                break
        if valmis is not None:
            print(valmis)
            abs_coord.add(valmis[1])
            not_abs_coord.remove(valmis[1])
            print("Lisätty")
    print("VAlmis")

    c = set()
    for k in lista:
        for kk in k:
            c.add(kk)
    print(len(c))

    maxdis = 0
    print(abspos)
    for i in range(len(lista)):
        for j in range(i+1, len(lista)):
            p1 = abspos[i]
            p2 = abspos[j]
            dis = abs(p1[0]-p2[0]) + abs(p1[1]-p2[1]) + abs(p1[2]-p2[2])
            if dis > maxdis:
                maxdis = dis
    print(maxdis)

def sijainti(l1, l2, a1, b1):
    for rot in range(24):
        lrot = rotate_list(l2, rot)
        brot = rotate(b1, rot)
        relpos = (-brot[0]+a1[0], -brot[1]+a1[1], -brot[2]+a1[2])
        lshift = shift_list(lrot, relpos)
        # print(rot, brot, relpos)
        # print(shift_list([brot], relpos))
        tot = 0
        for k in l1:
            if k in lshift:
                tot += 1
        # print(rot, relpos)
        # print(rot, tot, len(l1), len(lshift))
        if tot >= 12:
            return lshift, relpos
    assert False, "virhe"


def shift_list(l, shift):
    l2 = list()
    for ll in l:
        k2 = (ll[0] + shift[0], ll[1] + shift[1], ll[2] + shift[2])
        l2.append(k2)
    return l2


def rotate_list(l, rot):
    l2 = list()
    for k in l:
        l2.append(rotate(k, rot))
    return l2


def rotate(coord, rot):
    if rot < 8:
        eka = coord[0]
        loput = (coord[1], coord[2])
    elif rot < 16:
        eka = coord[1]
        loput = (-coord[0], coord[2])
    else:
        eka = coord[2]
        loput = (coord[1], -coord[0])
    r = rot % 8
    if r > 3:
        eka = -eka
        loput = (-loput[0], loput[1])
        r -= 4
    if r == 1:
        loput = (loput[1], -loput[0])
    if r == 2:
        loput = (-loput[0], -loput[1])
    if r == 3:
        loput = (-loput[1], loput[0])

    return (eka, loput[0], loput[1])


def paallekkain(l1, l2):
    for a1 in l1:
        et = set()
        for a2 in l1:
            et.add(frozenset((abs(a1[0]-a2[0]), abs(a1[1]-a2[1]), abs(a1[2]-a2[2]))))

        for b1 in l2:
            bt = set()
            for b2 in l2:
                bt.add(frozenset((abs(b1[0]-b2[0]), abs(b1[1]-b2[1]), abs(b1[2]-b2[2]))))

            tot = 0
            for e1 in et:
                if e1 in bt:
                    tot += 1
            # print(tot)
            if tot >= 12:
                # print(tot)
                return True, a1, b1
    return False, None, None


def get_input():
    lista = []
    print("Anna syöte:", end="")
    edvali = False
    l = []
    while True:
        syote = input()
        if syote == "":
            if edvali:
                break
            edvali = True
        else:
            edvali = False
        if syote[:3] == "---":
            if len(l) > 0:
                lista.append(l)
            l = list()
        elif syote != "":
            ll = []
            for k in syote.split(","):
                ll.append(int(k))
            l.append(tuple(ll))
    lista.append(l)
    return lista


if __name__ == "__main__":
    main()
