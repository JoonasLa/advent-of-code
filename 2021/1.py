import numpy as np


def main():
    lista = get_input()
    tot = 0
    for i in range(1, len(lista)):
        if lista[i] > lista[i-1]:
            tot += 1
    print(tot)

    tot = 0
    for i in range(3, len(lista)):
        if lista[i] > lista[i-3]:
            tot += 1
    print(tot)


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(int(syote))
    return lista


if __name__ == "__main__":
    main()
