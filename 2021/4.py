import numpy as np


def main():
    lista, ruudut = get_input()
    print(lista)
    print(ruudut)
    v, n = etsi_voittaja(lista, ruudut)
    print(v)
    tot = 0
    for i in range(5):
        for j in range(5):
            if v[i][j] is not None:
                tot += v[i][j]
    print(tot * n)


def etsi_voittaja(lista, ruudut):
    numvoit = 0
    for n in lista:
        for ir, r in enumerate(ruudut):
            if r is None:
                continue
            for i in range(5):
                for j in range(5):
                    if r[i][j] == n:
                        r[i][j] = None
                        stop = False
                        for k in range(5):
                            if r[i][k] is not None:
                                break
                        else:
                            stop = True
                            if numvoit == len(ruudut)-1:
                                return r, n
                            else:
                                ruudut[ir] = None
                                numvoit += 1
                        if not stop:
                            for k in range(5):
                                if r[k][j] is not None:
                                    break
                            else:
                                if numvoit == len(ruudut)-1:
                                    return r, n
                                else:
                                    ruudut[ir] = None
                                    numvoit += 1


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista += syote.split(",")
    for i in range(len(lista)):
        lista[i] = int(lista[i])

    ruutulist = list()
    ruutu = list()
    while True:
        syote = input()
        if syote == "":
            if len(ruutu) > 0:
                ruutulist.append(ruutu)
                ruutu = list()
            else:
                break
        else:
            rivi = list()
            for i in range(5):
                rivi.append(int(syote[3*i:3*i+3]))
            ruutu.append(rivi)

    return lista, ruutulist


if __name__ == "__main__":
    main()
