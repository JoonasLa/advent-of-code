import numpy as np
import heapq

def main():
    leka = get_input()
    lista = list()
    for yi in range(5):
        for y in range(len(leka)):
            rivi = list()
            for xi in range(5):
                for x in range(len(leka[0])):
                    t = leka[y][x] + yi + xi
                    while t > 9:
                        t -= 9
                    rivi.append(t)
            lista.append(rivi)
    print((len(leka), len(leka[0])), (len(lista), len(lista[0])))

    kokeilut = list()
    l1 = list(lista)
    l1[0][0] = None
    heapq.heappush(kokeilut, (0, l1, 0, 0))
    while True:
        # print(kokeilut)
        tot, l1, x, y = heapq.heappop(kokeilut)
        for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1)):
            x2 = x + dx
            y2 = y + dy
            if not 0 <= x2 < len(lista[0]) or not 0 <= y2 < len(lista) or l1[y2][x2] is None:
                continue
            tot2 = tot + lista[y2][x2]
            if (x2, y2) == (len(lista[0])-1, len(lista)-1):
                print(tot2)
                return
            l2 = list(l1)
            l2[y2][x2] = None
            heapq.heappush(kokeilut, (tot2, l2, x2, y2))



def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        l2 = list()
        for k in syote:
            l2.append(int(k))
        lista.append(l2)
    return lista


if __name__ == "__main__":
    main()
