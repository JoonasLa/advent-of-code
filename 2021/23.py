import numpy as np
import heapq

COST = {"A":1, "B":10, "C":100, "D":1000}

def main():
    lista = get_input()
    print(lista)
    pisteet = set()
    a = set()
    b = set()
    c = set()
    d = set()
    for i in range(len(lista)):
        for j in range(len(lista[0])):
            l = lista[i][j]
            if l == ".":
                pisteet.add((j, i))
            if l == "A":
                a.add((j, i))
            if l == "B":
                b.add((j, i))
            if l == "C":
                c.add((j, i))
            if l == "D":
                d.add((j, i))
    pisteet = frozenset(pisteet)
    a = frozenset(a)
    b = frozenset(b)
    c = frozenset(c)
    d = frozenset(d)

    heap = list()
    s1 = (pisteet, a, b, c, d)
    print(s1)
    matka = move_cost(s1)
    heapq.heappush(heap, (matka, 0, s1))
    valmiit = set()
    lastp = 0
    while True:
        a, p, coord = heapq.heappop(heap)
        if p > lastp:
            print(a, p, len(heap), len(valmiit))
            print_list(coord)
            lastp = p
        # print_list(coord)
        # print(a, p)
        # input()

        if on_valmis(coord):
            print(p)
            break
        pis = coord[0]
        cost = 1
        for i in range(1, 5):
            let = coord[i]
            xm = i*2+1
            omavapaa = True
            for j in range(2, 6):
                if (xm, j) not in pis and (xm, j) not in let:
                    omavapaa = False
                    break
            for x, y in let:
                p1 = (x, y)
                et = {p1: 0}
                tark = {p1}
                while tark:
                    xx, yy = tark.pop()
                    d = et[(xx, yy)]
                    for dx, dy in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                        xxx, yyy = xx + dx, yy + dy
                        if (xxx, yyy) in pis:
                            if (xxx, yyy) not in et:
                                tark.add((xxx, yyy))
                                et[(xxx, yyy)] = d + cost
                            elif d + cost < et[(xxx, yyy)]:
                                et[(xxx, yyy)] = d + cost
                et.pop(p1)

                for xx, yy in et:
                    if (xx, yy) in ((3, 1), (5, 1), (7, 1), (9, 1)):
                        continue
                    if xx != xm:
                        if yy != 1:
                            continue
                    if y == 1:
                        if xx != xm:
                            continue
                    if xx == xm and not omavapaa:
                        continue

                    pis2 = set(pis)
                    pis2.remove((xx, yy))
                    pis2.add((x, y))
                    pis2 = frozenset(pis2)
                    c2 = set(let)
                    c2.remove((x, y))
                    c2.add((xx, yy))
                    c2 = frozenset(c2)
                    s2 = (pis2,) + coord[1:i] + (c2,) + coord[i+1:]
                    if s2 not in valmiit:
                        p2 = p + et[(xx, yy)]
                        a2 = p2 + move_cost(s2)
                        heapq.heappush(heap, (a2, p2, s2))
                        valmiit.add(s2)
            cost *= 10


def main2():
    lista = get_input()
    print_list(lista)
    totcost = 0
    while True:
        print_list(lista)
        print(totcost)

        while True:
            try:
                x, y = input("Lähtö").split(" ")
                x = int(x)
                y = int(y)
                break
            except (ValueError, IndexError):
                pass
        while True:
            try:
                x2, y2 = input("Maali").split(" ")
                x2 = int(x2)
                y2 = int(y2)
                break
            except (ValueError, IndexError):
                pass
        let = lista[y][x]
        totcost += (abs(y2-y) + abs(x2-x)) * COST[let]
        lista[y][x] = "."
        lista[y2][x2] = let


def print_list(lista):
    if len(lista) == 5:
        p, a, b, c, d = lista
        for y in range(7):
            for x in range(13):
                co = (x, y)
                if co in p:
                    print(".", end="")
                elif co in a:
                    print("A", end="")
                elif co in b:
                    print("B", end="")
                elif co in c:
                    print("C", end="")
                elif co in d:
                    print("D", end="")
                else:
                    print("#", end="")
            print()

    else:
        s = ""
        for i in range(len(lista)):
            for j in range(len(lista[0])):
                if lista[i][j] is None:
                    s += "#"
                else:
                    s += lista[i][j]
            s += "\n"
        print(s)


def on_valmis(coord):
    for i in range(1, 5):
        l = []
        for ii in range(2, 6):
            l.append((i*2+1, ii))
        if coord[i] != frozenset(l):
            return False
    return True


def move_cost(coord):
    cost = 1
    totcost = 0
    pis = coord[0]
    for i in range(1, 5):
        c = coord[i]
        xm = i * 2 + 1
        for x, y in c:
            if x != xm:
                totcost += (abs(xm - x) + abs(y - 1)) * cost
            if xm > x:
                dx = 1
            else:
                dx = -1
            xx = x
            while xx != xm:
                if (xx, 1) not in pis and (xx, 1) not in c:
                    totcost += 100
                xx += dx

        for j in range(2, 6):
            if (xm, j) not in c:
                totcost += cost * (j-1)

        vaaraa = False
        for j in range(5, 0, -1):
            if (xm, j) not in pis and (xm, j) not in c:
                vaaraa = True
                totcost += cost*2
            elif vaaraa and (xm, j) in c:
                totcost += cost * (j+1)*2
        cost *= 10
    return totcost

def get_input():
    lista = []
    print("Anna syöte:", end="")
    lisa = ["  #D#C#B#A#", "  #D#B#A#C#"]
    ii = 0
    while True:
        syote = input()
        if syote == "":
            break
        l = [None]*13
        for i in range(len(syote)):
            if syote[i] == " " or syote[i] == "#":
                continue
            l[i] = syote[i]
        lista.append(l)
        ii += 1
        if ii == 3:
            for syote in lisa:
                l = [None] * 13
                for i in range(len(syote)):
                    if syote[i] == " " or syote[i] == "#":
                        continue
                    l[i] = syote[i]
                lista.append(l)
    return lista


if __name__ == "__main__":
    main()
