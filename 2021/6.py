import numpy as np
from numba import njit

def main():
    lista = get_input()
    splitnum = dict()
    for numleft in range(270):
        if numleft < 9:
            splitnum[numleft] = 1
        else:
            tot = 1
            for n2 in range(numleft - 9, -1, -7):
                tot += splitnum[n2]
            splitnum[numleft] = tot

    tot = 0
    for k in lista:
        tot += splitnum[256 + (8-k)]


    print(tot)


def get_input():
    lista = []
    s = ""
    while True:
        syote = input("Anna syöte:")
        if syote == "":
            break
        s += syote

    lista = s.split(",")
    for i in range(len(lista)):
        lista[i] = int(lista[i])
    return lista


if __name__ == "__main__":
    main()
