import numpy as np


def main():
    ruutu, taitos = get_input()
    print(ruutu)
    print(taitos)
    for suunta, kohta in taitos:
        if suunta == "x":
            for i in range(1, ruutu.shape[1] - kohta):
                for y in range(ruutu.shape[0]):
                    ruutu[y, kohta - i] += ruutu[y, kohta + i]
            ruutu = ruutu[:, :kohta]
        elif suunta == "y":
            for i in range(1, ruutu.shape[0]-kohta):
                for x in range(ruutu.shape[1]):
                    ruutu[kohta - i, x] += ruutu[kohta + i, x]
            ruutu = ruutu[:kohta, :]

    print(ruutu)
    print(np.sum(ruutu >= 1))
    s = ""
    for y in range(ruutu.shape[0]):
        for x in range(ruutu.shape[1]):
            if ruutu[y, x] > 0:
                s += "#"
            else:
                 s += " "
        s += "\n"
    print(s)




def get_input():
    lista = []
    print("Anna syöte:", end="")
    maxx1 = 0
    maxx2 = 0
    vaihe = 0
    taitos = list()
    while True:
        syote = input()
        if syote == "":
            vaihe += 1
            if vaihe == 2:
                break
            continue
        if vaihe == 0:
            x1, x2 = syote.split(",")
            x1, x2 = int(x1), int(x2)
            if x1 > maxx1:
                maxx1 = x1
            if x2 > maxx2:
                maxx2 = x2
            lista.append((x1, x2))
        else:
            t1, t2 = syote.split("=")
            taitos.append((t1[-1], int(t2)))
    print(maxx1, maxx2)
    ruutu = np.zeros((maxx2+1, maxx1+1), dtype=int)
    for x, y in lista:
        ruutu[y, x] = 1
    return ruutu, taitos


if __name__ == "__main__":
    main()
