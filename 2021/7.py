import numpy as np


def main():
    lista = get_input()
    mintot = np.inf
    mink = None
    for k in range(min(lista), max(lista)):
        tot = 0
        for a in lista:
            n = abs(a-k)
            tot += n*(n+1) // 2
        if tot < mintot:
            mintot = tot
            mink = k
    print(mink, mintot)

def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista += syote.split(",")
    for i in range(len(lista)):
        lista[i] = int(lista[i])
    return lista


if __name__ == "__main__":
    main()
