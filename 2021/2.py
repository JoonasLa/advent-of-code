import numpy as np


def main():
    lista = get_input()
    h1 = 0
    d = 0
    aim = 0
    for s, i in lista:
        if s == "forward":
            h1 += i
            d += aim * i
        elif s == "up":
            aim -= i
        elif s == "down":
            aim += i
    print(h1*d)


def get_input():
    lista = []
    while True:
        syote = input("Anna syöte:")
        if syote == "":
            break
        t = syote.split(" ")
        lista.append((t[0], int(t[1])))
    return lista


if __name__ == "__main__":
    main()
