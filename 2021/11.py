import numpy as np


def main():
    lista = get_input()
    print(lista)
    flashes = 0
    for step in range(10000):
        print(step)
        lista += 1
        while True:
            anyflash = False
            for i in range(10):
                for j in range(10):
                    if lista[i, j] > 9:
                        flashes += 1
                        lista[i, j] = 0
                        anyflash = True
                        for ii in range(max(0, i-1), min(10, i+2)):
                            for jj in range(max(0, j-1), min(10, j+2)):
                                if lista[ii, jj] != 0:
                                    lista[ii, jj] += 1
            if not anyflash:
                break
        if np.all(lista == 0):
            print("Pysähtyi", step)
            break
    print(flashes)

def get_input():
    lista = np.zeros((10, 10), dtype=int)
    print("Anna syöte:", end="")
    for i in range(10):
        syote = input()
        for j in range(10):
            lista[i, j] = int(syote[j])
    return lista


if __name__ == "__main__":
    main()
