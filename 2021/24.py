import numpy as np


def main():
    lista = get_input()
    segs = list()
    lasti = 0
    for i in range(1, len(lista)):
        if lista[i][0] == "inp":
            segs.append(lista[lasti:i])
            lasti = i
    segs.append(lista[lasti:])
    maxsama = np.inf
    kolmos = []
    for seg in segs:
        k3 = list()
        kaikkisamaa = True
        samaa = 0
        for com in seg:
            print(com)
            if kaikkisamaa and com[:2] == segs[0][samaa][:2]:
                samaa += 1
            else:
                kaikkisamaa = False
            if len(com) < 3:
                k3.append(" ")
            else:
                k3.append(com[2])
        if samaa < maxsama:
            maxsama = samaa
        print("#################")
        kolmos.append(k3)
    for k3 in kolmos:
        for d in k3:
            print(f"{d:3s}", end="||")
        print()
    print("Yhteistä", maxsama)

    print(len(segs))
    pos = {0:0}
    for segi in range(14):
        print(segi, len(pos))
        # print(pos.keys())
        coms = segs[segi]
        pos2 = dict()
        for key in pos:
            z = key
            for m in range(1, 10):
                ram = {"w":m, "z": z, "x":0, "y":0}
                model = pos[key]*10+m
                for com in coms[1:]:
                    suorita_com(com, ram)
                z2 = ram["z"]
                if (z2 not in pos2 or pos2[z2] > model) and z2 < 26 ** (14-segi-1):
                    pos2[z2] = model

        pos = pos2
    print("Valmis")
    print(pos)
    print(pos[0])


"""
['inp', 'w']
['mul', 'x', '0']
['add', 'x', 'z']
['mod', 'x', '26']
['div', 'z', '26'] x = z0 % 26, z = z0 // 26
['add', 'x', '-14'] x = z0 % 26 - 14
['eql', 'x', 'w']
['eql', 'x', '0'] x = z0 & 26 - 14 != w
['mul', 'y', '0']
['add', 'y', '25']
['mul', 'y', 'x']
['add', 'y', '1'] y = 26 tai 1
['mul', 'z', 'y'] 
['mul', 'y', '0']
['add', 'y', 'w']
['add', 'y', '3']
['mul', 'y', 'x']
['add', 'z', 'y']
"""

def suorita_com(com, ram):
    c = com[0]
    if c == "inp":
        ram[com[1]] = inp[inputi]
        inputi += 1
        return
    if com[2] in ram:
        v2 = ram[com[2]]
    else:
        v2 = int(com[2])
    v1 = ram[com[1]]
    if c == "add":
        ram[com[1]] = v1 + v2
    elif c == "mul":
        ram[com[1]] = v1 * v2
    elif c == "div":
        ram[com[1]] = v1 // v2
    elif c == "mod":
        ram[com[1]] = v1 % v2
    elif c == "eql":
        if v1 == v2:
            ram[com[1]] = 1
        else:
            ram[com[1]] = 0

def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote.split(" "))
    return lista


if __name__ == "__main__":
    main()


"""
inp w 
mul x 0 x 0
add x z x = 0
mod x 26 x = 0
div z 1 z = 0
add x 13 x = 13
eql x w x = 0
eql x 0 x = 1
mul y 0 y = 0
add y 25 y = 25
mul y x 
add y 1
mul z y
mul y 0
add y w
add y 14
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 12
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 8
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 11
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 5
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x 0
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 4
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 15
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 10
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -13
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 13
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 10
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 16
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -9
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 5
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 11
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 6
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 13
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 13
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -14
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 6
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -3
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 7
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -2
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 13
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -14
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 3
mul y x
add z y

"""