import numpy as np


def main():
    lista = get_input()
    print(magnitude(lista[0]))
    l = lista[0]
    for i in range(1, len(lista)):
        l = summaa(l, lista[i])
        print("Välitulos", i, print_list(l))
    print("Lopputulos", print_list(l))
    print(magnitude(l))

    maxmag = 0
    for i in range(len(lista)):
        for j in range(len(lista)):
            if i == j:
                continue
            mag = magnitude(summaa(lista[i], lista[j]))
            if mag > maxmag:
                maxmag = mag
    print(maxmag)

def magnitude(l):
    while True:
        for i in range(len(l)):
            if isinstance(l[i], int) and isinstance(l[i+1], int):
                l = l[:i-1] + [3*l[i] + 2 * l[i+1]] + l[i+3:]
                break
        if len(l) == 1:
            return l[0]

def summaa(l1, l2):
    l3 = ["["] + l1 + l2 + ["]"]
    # print("summa", print_list(l3))
    return iteroi(l3)


def iteroi(l):
    while True:
        d = 0
        for i, n in enumerate(l):
            if n == "[":
                d += 1
            elif n == "]":
                d -= 1
            if d == 5:
                eka, toka = l[i+1], l[i+2]
                for j in range(i-1, -1, -1):
                    if isinstance(l[j], int):
                        l[j] += eka
                        break
                for j in range(i + 3, len(l)):
                    if isinstance(l[j], int):
                        l[j] += toka
                        break
                l = l[:i] + [0] + l[i+4:]
                # print("explode", print_list(l))
                break
        else:
            for i, n in enumerate(l):
                if isinstance(n, int) and n > 9:
                    eka = n // 2
                    toka = n - eka
                    l = l[:i] + ["[", eka, toka, "]"] + l[i+1:]
                    # print("split", print_list(l))
                    break
            else:
                break
    return l


def print_list(l):
    s = ""
    for i in range(len(l)):
        s += str(l[i])
        if i < len(l)-1:
            if isinstance(l[i], int) and (isinstance(l[i+1], int) or l[i+1] == "["):
                s += ","
            if l[i] == "]" and (l[i+1] == "[" or isinstance(l[i+1], int)):
                s += ","
    return s


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        l = list()
        for s in syote:
            if s == "[" or s == "]":
                l.append(s)
            elif str.isdigit(s):
                l.append(int(s))
        lista.append(l)
    return lista


if __name__ == "__main__":
    main()
