import numpy as np


def main():
    testi()

    lista = get_input()
    print(lista[0])
    print(len(lista))
    alueet = set()
    cube = np.zeros((101, 101, 101), dtype=bool)
    for i in range(len(lista)):
        on, a1 = lista[i]
        alueet2 = set()
        for a2 in alueet:
            for al2 in pilko_alue(a2, a1):
                alueet2.add(al2)
        if on:
            alueet2.add(a1)

        alueet = alueet2
        print("Iteraatio", i, "Alueita", len(alueet))
        for al in alueet:
            koko = (al[0][1] - al[0][0] + 1) * (al[1][1] - al[1][0] + 1) * (al[2][1] - al[2][0] + 1)
            # print(al, koko)
        cube[a1[0][0]+50:a1[0][1]+51, a1[1][0]+50:a1[1][1]+51, a1[2][0]+50:a1[2][1]+51] = on
        c2 = np.array(cube)
        for al in alueet:
            assert np.all(c2[al[0][0]+50:al[0][1]+51, al[1][0]+50:al[1][1]+51, al[2][0]+50:al[2][1]+51])
            c2[al[0][0] + 50:al[0][1] + 51, al[1][0] + 50:al[1][1] + 51, al[2][0] + 50:al[2][1] + 51] = False
        if np.any(c2):
            assert False

    tot = 0
    print("Lopulliset alueet")
    for al in alueet:
        koko = (al[0][1]-al[0][0]+1)*(al[1][1]-al[1][0]+1)*(al[2][1]-al[2][0]+1)
        assert al[0][0] <= al[0][1] and al[1][0] <= al[1][1] and al[2][0] <= al[2][1], "virhe"
        print(al, koko)
        tot += koko
    print(tot, len(alueet))


def testi():
    al1 = ((-1, 1), (-1, 1), (-1, 1))
    for i1 in range(-1, 2):
        for i2 in range(-1, 2):
            for i3 in range(-1, 2):
                ap = ((i1, i1), (i2, i2), (i3, i3))
                pilko_alue(al1, ap)


def pilko_alue(a, ap):
    al = list()
    if a[0][1] > ap[0][1]:
        al.append(((max(a[0][0], ap[0][1]+1), a[0][1]), a[1], a[2]))
        # print("Pilkko1")
    if a[0][0] < ap[0][0]:
        al.append(((a[0][0], min(a[0][1], ap[0][0] - 1)), a[1], a[2]))
        # print("Pilkko2")
    if a[1][1] > ap[1][1] and (ap[0][0] <= a[0][0] <= ap[0][1] or ap[0][0] <= a[0][1] <= ap[0][1] or a[0][0] <= ap[0][0] <= a[0][1] or a[0][0] <= ap[0][1] <= a[0][1]):
        al.append(((max(a[0][0], ap[0][0]), min(a[0][1], ap[0][1])), (max(a[1][0], ap[1][1]+1), a[1][1]), a[2]))
        # print("Pilkko3")
    if a[1][0] < ap[1][0] and (ap[0][0] <= a[0][0] <= ap[0][1] or ap[0][0] <= a[0][1] <= ap[0][1] or a[0][0] <= ap[0][0] <= a[0][1] or a[0][0] <= ap[0][1] <= a[0][1]):
        al.append(((max(a[0][0], ap[0][0]), min(a[0][1], ap[0][1])), (a[1][0], min(a[1][1], ap[1][0] - 1)), a[2]))
        # print("Pilkko4")
    if a[2][1] > ap[2][1] and (ap[0][0] <= a[0][0] <= ap[0][1] or ap[0][0] <= a[0][1] <= ap[0][1] or a[0][0] <= ap[0][0] <= a[0][1] or a[0][0] <= ap[0][1] <= a[0][1]) and (ap[1][0] <= a[1][0] <= ap[1][1] or ap[1][0] <= a[1][1] <= ap[1][1] or a[1][0] <= ap[1][0] <= a[1][1] or a[1][0] <= ap[1][1] <= a[1][1]):
        al.append(((max(a[0][0], ap[0][0]), min(a[0][1], ap[0][1])), (max(a[1][0], ap[1][0]), min(a[1][1], ap[1][1])), (max(a[2][0], ap[2][1]+1), a[2][1])))
        # print("Pilkko5")
    if a[2][0] < ap[2][0] and (ap[0][0] <= a[0][0] <= ap[0][1] or ap[0][0] <= a[0][1] <= ap[0][1] or a[0][0] <= ap[0][0] <= a[0][1] or a[0][0] <= ap[0][1] <= a[0][1]) and (ap[1][0] <= a[1][0] <= ap[1][1] or ap[1][0] <= a[1][1] <= ap[1][1] or a[1][0] <= ap[1][0] <= a[1][1] or a[1][0] <= ap[1][1] <= a[1][1]):
        al.append(((max(a[0][0], ap[0][0]), min(a[0][1], ap[0][1])), (max(a[1][0], ap[1][0]), min(a[1][1], ap[1][1])), (a[2][0], min(a[2][1], ap[2][0] - 1))))
        # print("Pilkko6")
    # print(f"Alue {a} Pikotaan alueella {ap} Tulos:")
    return al


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        onf, xyz = syote.split(" ")
        xyz = xyz.split(",")
        l = list()
        for i in range(3):
            eka, toka = xyz[i][2:].split("..")
            l.append((int(eka), int(toka)))
        l = tuple(l)
        lista.append((onf == "on", l))
    return lista


if __name__ == "__main__":
    main()
