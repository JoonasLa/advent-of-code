import numpy as np


def main():
    lista = get_input()
    print(rekursio(lista, {'start'}, 'start', False))


def rekursio(lista, kayty, cur, kahd):
    tot = 0
    for n in lista[cur]:
        if n == "end":
            tot += 1
        elif not str.islower(n):
            tot += rekursio(lista, kayty, n, kahd)
        else:
            if n in kayty:
                if not kahd and n != 'start':
                    k2 = set(kayty)
                    k2.add(n)
                    tot += rekursio(lista, k2, n, True)
            else:
                k2 = set(kayty)
                k2.add(n)
                tot += rekursio(lista, k2, n, kahd)
    return tot

def get_input():
    lista = dict()
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        s1, s2 = syote.split("-")
        if s1 not in lista:
            lista[s1] = list()
        if s2 not in lista:
            lista[s2] = list()
        lista[s1].append(s2)
        lista[s2].append(s1)
    return lista


if __name__ == "__main__":
    main()
