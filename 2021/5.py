import numpy as np


def main():
    lista = get_input()

    ruutu = np.zeros((1000, 1000), dtype=int)
    for (x1, y1), (x2, y2) in lista:
        dx = 0
        if x2 > x1:
            dx = 1
        if x2 < x1:
            dx = -1
        dy = 0
        if y2 > y1:
            dy = 1
        if y2 < y1:
            dy = -1
        if dx == 0 and dy == 0:
            continue

        if dx == 0:
            for y in range(y1, y2 + dy, dy):
                ruutu[y][x1] += 1
        elif dy == 0:
            for x in range(x1, x2 + dx, dx):
                ruutu[y1][x] += 1
        else:
            for i in range(abs(x2 - x1) + 1):
                ruutu[y1 + dy*i][x1 + dx*i] += 1
    print(np.sum(ruutu >= 2))


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        p1, p2 = syote.split(" -> ")
        x1, y1 = p1.split(",")
        x2, y2 = p2.split(",")
        lista.append(((int(x1), int(y1)), (int(x2), int(y2))))

    return lista


if __name__ == "__main__":
    main()
