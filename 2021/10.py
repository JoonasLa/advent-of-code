import numpy as np

PARIT = [("(", ")"), ("[", "]"), ("{", "}"), ("<", ">")]
P2 = {"(": ")", "[": "]", "{": "}", "<": ">"}
ALUT = {"(", "[", "{", "<"}
LOPUT = {")", "]", "}", ">"}
PISTEET = {")":3, "]":57, "}":1197, ">":25137}

def main():
    lista = get_input()
    incomplete = list()
    tot = 0
    s2 = list(lista)
    for ii, sarja in enumerate(lista):
        while True:
            for i in range(len(sarja)-1):
                if (sarja[i], sarja[i+1]) in PARIT:
                    sarja = sarja[:i] + sarja[i+2:]
                    break
            else:
                break
        for i in range(len(sarja)-1):
            if sarja[i] in ALUT and sarja[i+1] in LOPUT:
                if sarja[i+1] != P2[sarja[i]]:
                    tot += PISTEET[sarja[i+1]]
                    break
        else:
            incomplete.append(sarja)
    print(tot)

    tot = []
    p2 = {")":1, "]":2, "}":3, ">":4}
    for s in incomplete:
        l2 = list()
        for ss in s[::-1]:
            l2.append(P2[ss])
        t = 0
        for k in l2:
            t *= 5
            t += p2[k]
        tot.append(t)
    print(np.median(tot))

def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(list(syote))
    return lista


if __name__ == "__main__":
    main()
