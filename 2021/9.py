import numpy as np


def main():
    lista = get_input()
    tot = 0
    basins = list()
    for y in range(len(lista)):
        for x in range(len(lista[0])):
            pien = True
            for xx in range(max(0, x-1), min(x+2, len(lista[0]))):
                for yy in range(max(0, y-1), min(y+2, len(lista))):
                    if abs(xx-x) + abs(yy-y) != 1:
                        continue
                    if lista[y][x] >= lista[yy][xx]:
                        pien = False
                        break
            if pien:
                tot += lista[y][x] + 1
                basins.append(etsi_basin(lista, x, y))
    print(tot)
    t2 = 1
    for l in sorted(basins, reverse=True)[:3]:
        t2 *= l
    print(t2)

def etsi_basin(lista, x, y):
    etsi = set(((x, y),))
    loyd = set(((x, y),))
    while len(etsi) > 0:
        x, y = etsi.pop()
        for xx in range(max(0, x - 1), min(x + 2, len(lista[0]))):
            for yy in range(max(0, y - 1), min(y + 2, len(lista))):
                if abs(xx - x) + abs(yy - y) != 1:
                    continue
                if lista[yy][xx] > lista[y][x] and lista[yy][xx] != 9 and (xx, yy) not in loyd:
                    etsi.add((xx, yy))
                    loyd.add((xx, yy))
    return len(loyd)


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        l = list()
        for k in syote:
            l.append(int(k))
        lista.append(l)
    return lista


if __name__ == "__main__":
    main()
