import numpy as np


def main():

    xt = (48, 70)
    yt = (-189, -148)
    tot = 0
    for vx in range(1, 71):
        for vy in range(-189, 191):
            x, y = 0, 0
            vx1 = vx
            vy1 = vy
            while True:
                x, y = x + vx1, y + vy1
                if x > xt[1] or y < yt[0]:
                    break
                if xt[0] <= x <= xt[1] and yt[0] <= y <= yt[1]:
                    tot += 1
                    break
                if vx1 > 0:
                    vx1 -= 1
                vy1 -= 1
    print(tot)



if __name__ == "__main__":
    main()
