import numpy as np


def main():
    lista = get_input()
    tot = 0
    for a1, a2 in lista:
        tot += ratkaise(a1, a2)
    print(tot)

def ratkaise_helppo(a2):
    tot = 0
    for k in a2:
        if len(k) in (2, 3, 4, 7):
            tot += 1
    return tot

def ratkaise(koodi, vastaus):

    let = ('a', 'b', 'c', 'd', 'e', 'f', 'g')
    vast = dict()
    num = dict()
    # nums 2:1, 3:7, 4:4, 5:2,3,5 6:0,6,9, 7:8

    # f
    for l in let:
        numput = 0
        for k in koodi:
            if l not in k:
                numput += 1
                if numput > 1:
                    break
        if numput == 1:
            vast['f'] = l

    #c
    for k in koodi:
        if len(k) == 2:
            for kk in k:
                if kk != vast['f']:
                    vast['c'] = kk
                    break

    #b
    for k in koodi:
        if vast['f'] not in k:
            for l in let:
                if l != vast['f'] and l not in k:
                    vast['b'] = l
                    break

    #e
    for l in let:
        numput = 0
        for k in koodi:
            if l not in k:
                numput += 1
        if numput == 6:
            vast['e'] = l
            break
        print(l, numput)

    nume = ""
    for k in vastaus:
        if len(k) == 2:
            nume += "1"
        elif len(k) == 3:
            nume += "7"
        elif len(k) == 4:
            nume += "4"
        elif len(k) == 7:
            nume += "8"
        elif len(k) == 5:
            if vast['c'] not in k:
                nume += "5"
            elif vast['f'] not in k:
                nume += "2"
            else:
                nume += "3"
        elif len(k) == 6:
            if vast['c'] not in k:
                nume += "6"
            elif vast['e'] not in k:
                nume += "9"
            else:
                nume += "0"
    print(vast)
    print(num)
    return int(nume)

def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        a1, a2 = syote.split(" | ")
        l1 = list()
        for k in a1.split(" "):
            l1.append(set(k))
        l2 = list()
        for k in a2.split(" "):
            l2.append(set(k))
        lista.append([l1, l2])
    return lista


if __name__ == "__main__":
    main()
