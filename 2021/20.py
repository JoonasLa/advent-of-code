import numpy as np


def main():
    koodi, lista = get_input()

    k2 = []
    ruutu = np.zeros((len(lista), len(lista[0])), dtype=bool)
    for i in range(len(lista)):
        for j in range(len(lista[0])):
            ruutu[i, j] = lista[i][j] == "#"

    for i in range(len(koodi)):
        k2.append(koodi[i] == "#")
    koodi = k2
    r2 = np.zeros((ruutu.shape[0]+100, ruutu.shape[1]+100), dtype=bool)
    r2[50:-50, 50:-50] = ruutu
    ruutu = r2

    for step in range(50):
        print(step)
        r2 = np.zeros_like(ruutu)
        for y in range(ruutu.shape[0]):
            for x in range(ruutu.shape[1]):
                b = ""
                for yy in range(y-1, y+2):
                    for xx in range(x-1, x+2):
                        if not 0 <= yy < ruutu.shape[0] or not 0 <= xx < ruutu.shape[1]:
                            if step % 2 == 1:
                                b += "1"
                            else:
                                b += "0"
                        else:
                            if ruutu[yy, xx]:
                                b += "1"
                            else:
                                b += "0"
                r2[y, x] = koodi[int(b, 2)]
        ruutu = r2

    print(np.sum(ruutu))



def get_input():
    lista = []
    print("Anna syöte:", end="")
    vaihe = 0
    koodi = ""
    while True:
        syote = input()
        if syote == "":
            vaihe += 1
            if vaihe == 2:
                break
        elif vaihe == 0:
            koodi += syote
        elif vaihe == 1:
            lista.append(syote)
    return koodi, lista


if __name__ == "__main__":
    main()
