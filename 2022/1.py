import numpy as np


def main():
    lista = get_input()
    maxlis = 0
    for lis in lista:
        if sum(lis) > maxlis:
            maxlis = sum(lis)
    print(maxlis)

    lis2 = sorted(lista, key=lambda x:sum(x))
    print(sum(lis2[-3]) + sum(lis2[-2])+sum(lis2[-1]))



def get_input():
    lista = []
    print("Anna syöte:", end="")
    tau = False
    lis = list()
    while True:
        syote = input()
        if syote == "":
            if tau:
                break
            else:
                lista.append(lis)
                lis = list()
            tau = True

        else:
            tau = False
            lis.append(int(syote))
    return lista


if __name__ == "__main__":
    main()
