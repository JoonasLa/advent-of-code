import numpy as np


def main():
    lista = get_input()
    print(lista)

    monkey_items = dict()
    for monkey, startit, oper, test, truethrow, falsethrow in lista:
        monkey_items[monkey] = list()
        for item in startit:
            monkey_items[monkey].append(item)
    numthrows = [0] * len(monkey_items)

    for rou in range(10000):
        for monkey, startit, oper, test, truethrow, falsethrow in lista:
            ml = monkey_items[monkey]
            for item in ml:
                num = oper(item)
                print(monkey, item, num, test(num))
                if test(num):
                    monkey_items[truethrow].append(num)
                else:
                    monkey_items[falsethrow].append(num)
                numthrows[monkey] += 1
            monkey_items[monkey] = list()
        print(rou, monkey_items)

    print(numthrows)
    numthrows = sorted(numthrows)
    tot = numthrows[-1] * numthrows[-2]

    print(tot)

    print(lista[0][2](1))


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        monkey = input()
        if monkey == "":
            break
        startit = input()
        oper = input()
        test = input()
        truethrow = input()
        flasethrow = input()
        _ = input()

        monkey = int(monkey[:-1].split(" ")[1])
        startit = startit.strip().split(":")[1]
        startit = eval("[" + startit + "]")
        if "*" in oper:
            opnum = oper.rsplit("*")[1]
            if "old" in opnum:
                oper = lambda x: (x**2) % (2*3*5*7*11*13*17*19)
            else:
                opnum = int(opnum)
                oper = mult_func(opnum)
        else:
            opnum = int(oper.rsplit("+")[1])
            oper = sum_func(opnum)
        testnum = int(test.rsplit(" ", 1)[1])
        test = divisible(testnum)
        truethrow = int(truethrow.rsplit(" ", 1)[1])
        flasethrow = int(flasethrow.rsplit(" ", 1)[1])
        lista.append([monkey, startit, oper, test, truethrow, flasethrow])

    return lista


def sum_func(opnum):
    def func(x):
        return (x + opnum) % (2*3*5*7*11*13*17*19)
    return func


def mult_func(opnum):
    def func(x):
        return (x * opnum) % (2*3*5*7*11*13*17*19)
    return func

def divisible(opnum):
    def func(x):
        return x % opnum == 0
    return func

if __name__ == "__main__":
    main()
