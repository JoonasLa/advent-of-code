import numpy as np


def main():
    lista = get_input()
    print(lista)
    maxx, maxy = 0, 0
    minx, miny = np.inf, np.inf
    for rock in lista:
        for x, y in rock:
            maxx = max(maxx, x)
            maxy = max(maxy, y)
            minx = min(minx, x)
            miny = min(miny, y)
    ruutu = list()
    for i in range(180):
        ruutu.append(["."]*600)
    for rock in lista:
        for i in range(len(rock)-1):
            x1, y1 = rock[i]
            x2, y2 = rock[i+1]
            num = max(abs(x2-x1), abs(y2-y1)) + 1
            xlist = np.linspace(x1, x2, num)
            ylist = np.linspace(y1, y2, num)
            # print(xlist)
            # print(ylist)
            for ii in range(num):
                ruutu[int(ylist[ii])][int(xlist[ii])] = "#"


    tot = 0

    sandx, sandy = 400, 0
    while True:
        x, y = sandx, sandy
        while True:
            if y == 178 or not (1 <= x < 499):
                break
            if ruutu[y+1][x] == ".":
                y += 1
            elif ruutu[y+1][x-1] == ".":
                x -= 1
                y += 1
            elif ruutu[y+1][x+1] == ".":
                x += 1
                y += 1
            else:
                break

        print("lopetti", x, y)
        if y == 178 or not 1 <= x < 499:
            break
        ruutu[y][x] = "o"
        print(x, y, "lisätty")
        tot += 1
    print(tot)

    for i in range(600):
        ruutu[maxy + 2][i] = "#"

    while True:
        x, y = sandx, sandy
        if ruutu[y+1][x] == "o" and ruutu[y+1][x+1] == "o" and ruutu[y+1][x-1] == "o":
            tot += 1
            break
        while True:
            if y == 178 or not (1 <= x < 599):
                break
            if ruutu[y+1][x] == ".":
                y += 1
            elif ruutu[y+1][x-1] == ".":
                x -= 1
                y += 1
            elif ruutu[y+1][x+1] == ".":
                x += 1
                y += 1
            else:
                break

        print("lopetti", x, y)
        ruutu[y][x] = "o"
        print(x, y, "lisätty")
        tot += 1
    print(tot)




def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lis = syote.split(" -> ")
        for i in range(len(lis)):
            lis[i] = lis[i].split(",")
            lis[i][0] = int(lis[i][0])-100
            lis[i][1] = int(lis[i][1])
        lista.append(lis)
    return lista


if __name__ == "__main__":
    main()
