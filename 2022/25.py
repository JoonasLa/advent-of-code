import numpy as np


def main():
    lista = get_input()
    print(lista)

    tot = 0
    for snafu in lista:
        num = 0
        ker = 1
        for s in snafu[::-1]:
            if s == "-":
                n = -1
            elif s == "=":
                n = -2
            else:
                n = int(s)
            num += n * ker
            ker *= 5
        print(num)
        tot += num

    print(tot)
    nums = list()
    while tot > 0:
        nums.append(tot % 5)
        tot //= 5
    nums = nums[::-1]
    print(nums)

    while True:
        muutos = False
        for i in range(len(nums)):
            if nums[i] > 2:
                nums[i] -= 5
                if i == 0:
                    nums = [1] + nums
                else:
                    nums[i-1] += 1
                break
        else:
            break
    for i in range(len(nums)):
        n = nums[i]
        if n == -2:
            s = "="
        elif n == -1:
            s = "-"
        else:
            s = str(n)
        print(s, end="")




def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote)
    return lista


if __name__ == "__main__":
    main()
