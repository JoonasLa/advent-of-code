import numpy as np


def main():
    lista = get_input()
    print(lista)
    tot = 0
    for i in range(len(lista)):
        for j in range(len(lista[0])):
            found = False
            for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1)):
                vis = True
                for k in range(1, len(lista)):
                    x, y = j + dx * k, i + dy * k
                    if (not 0 <= x < len(lista[0])) or (not 0 <= y < len(lista)):
                        break
                    if lista[y][x] >= lista[i][j]:
                        vis = False
                        break
                if vis:
                    found = True
                    break
            if found:
                print(i, j, lista[i][j], "näkyy")
                tot += 1

    print(tot)

    maxd = 0
    for i in range(len(lista)):
        for j in range(len(lista[0])):
            dlist = list()
            for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1)):
                dist = 0
                for k in range(1, len(lista)):
                    x, y = j + dx * k, i + dy * k
                    y2, x2 = y - dy, x - dx
                    if (not 0 <= x < len(lista[0])) or (not 0 <= y < len(lista)):
                        break
                    dist += 1
                    if lista[y][x] >= lista[i][j]:
                        break

                dlist.append(dist)
            print(i, j, dlist)
            maxd = max(maxd, np.prod(dlist))
    print(maxd)


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lis = list()
        for s in syote:
            lis.append(int(s))
        lista.append(lis)
    return lista


if __name__ == "__main__":
    main()
