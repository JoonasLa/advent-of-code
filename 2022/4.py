import numpy as np


def main():
    lista = get_input()
    print(lista)
    tot = 0

    for (ee1, ee2), (tt1, tt2) in lista:
        if (tt1 <= ee1 and ee2 <= tt2) or (ee1 <= tt1 and tt2 <= ee2):
            tot += 1
    print(tot)

    tot = 0

    for (ee1, ee2), (tt1, tt2) in lista:
        if (tt1 <= ee1 <= tt2) or (ee1 <= tt1 <= ee2) or (tt1 <= ee2 <= tt2) or (ee1 <= tt2 <= ee2):
            tot += 1
    print(tot)


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        eka, toka = syote.split(",")
        ee1, ee2 = eka.split("-")
        tt1, tt2 = toka.split("-")
        lista.append(((int(ee1), int(ee2)), (int(tt1), int(tt2))))
    return lista


if __name__ == "__main__":
    main()
