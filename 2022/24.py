import numpy as np
import heapq

def main():
    lista = get_input()
    print(lista)

    print(len(lista), len(lista[0]))
    vapaat_dict = laske_vapaat(lista)
    print(vapaat_dict[1])

    startx, starty = 0, len(lista)-2
    heap = [(0, (startx, starty))]
    tark = {(0, startx, starty)}
    while len(heap) > 0:
        step, (x, y) = heapq.heappop(heap)
        print(step, x, y)
        step += 1
        stepdiv = step % ((len(lista)-2)*(len(lista[0])-2))
        vapaat = vapaat_dict[stepdiv]
        for dx, dy in ((0, 0), (1, 0), (-1, 0), (0, 1), (0, -1)):
            xx, yy = x + dx, y + dy
            if (xx, yy) == (len(lista[0])-3, -1):
                print("Maali löytyi", step)
                return
            if (xx, yy) != (startx, starty) and (not 0 <= xx < len(lista[0])-2 or not 0 <= yy < len(lista)-2 or (xx, yy) not in vapaat):
                continue
            if (step, xx, yy) in tark:
                continue
            tark.add((step, xx, yy))
            heapq.heappush(heap, (step, (xx, yy)))


def main2():
    lista = get_input()
    print(lista)

    print(len(lista), len(lista[0]))
    vapaat_dict = laske_vapaat(lista)
    print(vapaat_dict[1])

    startx, starty = 0, len(lista)-2
    goalx, goaly = len(lista[0]) - 3, -1

    heap = [(0, 0, (startx, starty))]
    tark = {(0, 0, startx, starty)}
    while len(heap) > 0:
        step, phase, (x, y) = heapq.heappop(heap)
        print(step, phase, x, y)
        step += 1
        stepdiv = step % ((len(lista)-2)*(len(lista[0])-2))
        vapaat = vapaat_dict[stepdiv]
        for dx, dy in ((0, 0), (1, 0), (-1, 0), (0, 1), (0, -1)):
            xx, yy = x + dx, y + dy
            ph2 = phase
            if (xx, yy) == (goalx, goaly):
                if phase == 2:
                    print("Maali löytyi", step)
                    return
                if phase == 0:
                    ph2 = 1
            if (xx, yy) == (startx, starty):
                if phase == 1:
                    ph2 = 2
            if not ((xx, yy) == (startx, starty) or (xx, yy) == (goalx, goaly)) and (not 0 <= xx < len(lista[0])-2 or not 0 <= yy < len(lista)-2 or (xx, yy) not in vapaat):
                continue
            if (step, ph2, xx, yy) in tark:
                continue
            tark.add((step, ph2, xx, yy))
            heapq.heappush(heap, (step, ph2, (xx, yy)))



def laske_vapaat(lista):
    vapaat = dict()
    uplist = list()
    downlist = list()
    leftlist = list()
    rightlist = list()
    for y in range(len(lista)):
        for x in range(len(lista[0])):
            if lista[y][x] == "^":
                uplist.append((x-1, len(lista) - y-2))
            if lista[y][x] == "v":
                downlist.append((x-1, len(lista) - y-2))
            if lista[y][x] == ">":
                rightlist.append((x-1, len(lista) - y-2))
            if lista[y][x] == "<":
                leftlist.append((x-1, len(lista) - y-2))
    for step in range((len(lista)-2)*(len(lista[0])-2)):
        vp = set()
        for y in range(0, len(lista)-2):
            for x in range(0, len(lista[0])-2):
                vp.add((x, y))
        for lis in (uplist, downlist, rightlist, leftlist):
            for x, y in lis:
                if (x, y) in vp:
                    vp.remove((x, y))
        vp.add((0, len(lista)-2))
        vp.add((len(lista[0])-3, -1))
        vapaat[step] = vp
        for lis, dx, dy in ((uplist, 0, 1), (downlist, 0, -1), (leftlist, -1, 0), (rightlist, 1, 0)):
            for i in range(len(lis)):
                lis[i] = ((lis[i][0] + dx) % (len(lista[0]) - 2), (lis[i][1] + dy) % (len(lista) - 2))
    return vapaat


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote)
    return lista


if __name__ == "__main__":
    main2()
