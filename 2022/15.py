import numpy as np


def main():
    lista = get_input()
    print(lista)

    row = 2000000
    not_in_row = list()
    for senx, seny, beax, beay in lista:
        dist = abs(senx - beax) + abs(seny - beay)
        numl = dist - abs(row - seny)
        if numl >= 0:
            not_in_row.append((senx - numl, senx + numl))
    tot = 0
    counted = list()
    for i, (l1, l2) in enumerate(not_in_row):
        for l3, l4 in counted:
            if l3 is None:
                continue
            if l3 <= l1 <= l4:
                l1 = l4 + 1
            if l3 <= l2 <= l4:
                l2 = l3 - 1

        if l1 <= l2:
            tot += l2-l1+1
            counted.append((l1, l2))

        for i, (l1, l2) in enumerate(counted):
            if l1 is None:
                continue
            for j, (l3, l4) in enumerate(counted[:i]):
                if l3 is None:
                    continue
                if l1 <= l3 <= l4 <= l2:
                    counted[j] = None, None

    tot = 0
    for l1, l2 in counted:
        if l1 is None:
            continue
        tot += l2 - l1 + 1
    beac_coords = set()
    for senx, seny, beax, beay in lista:
        beac_coords.add((beax, beay))
    print(tot)
    for _, y in beac_coords:
        if y == row:
            tot -= 1

    print(not_in_row)
    print(counted)
    print(tot)

    row = 3042458

    not_in_row = list()
    for senx, seny, beax, beay in lista:
        dist = abs(senx - beax) + abs(seny - beay)
        numl = dist - abs(row - seny)
        if numl >= 0:
            not_in_row.append((senx - numl, senx + numl))
    tot = 0
    counted = list()
    for i, (l1, l2) in enumerate(not_in_row):
        for l3, l4 in counted:
            if l3 is None:
                continue
            if l3 <= l1 <= l4:
                l1 = l4 + 1
            if l3 <= l2 <= l4:
                l2 = l3 - 1

        if l1 <= l2:
            tot += l2 - l1 + 1
            counted.append((l1, l2))

        for i, (l1, l2) in enumerate(counted):
            if l1 is None:
                continue
            for j, (l3, l4) in enumerate(counted[:i]):
                if l3 is None:
                    continue
                if l1 <= l3 <= l4 <= l2:
                    counted[j] = None, None

    vaihd = set()
    for l1, l2 in counted:
        if l1 is None:
            continue
        l1 = max(l1, 0)
        l2 = min(l2, 4000000)
        if l1 in vaihd:
            vaihd.remove(l1)
        else:
            vaihd.add(l1 - 1)
        if l2 in vaihd:
            vaihd.remove(l2)
        else:
            vaihd.add(l2 + 1)
    print(vaihd)

    print(3012821 * 4000000 + row)



    return

    for row in range(4000000):
        not_in_row = list()
        for senx, seny, beax, beay in lista:
            dist = abs(senx - beax) + abs(seny - beay)
            numl = dist - abs(row - seny)
            if numl >= 0:
                not_in_row.append((senx - numl, senx + numl))
        tot = 0
        counted = list()
        for i, (l1, l2) in enumerate(not_in_row):
            for l3, l4 in counted:
                if l3 is None:
                    continue
                if l3 <= l1 <= l4:
                    l1 = l4 + 1
                if l3 <= l2 <= l4:
                    l2 = l3 - 1

            if l1 <= l2:
                tot += l2 - l1 + 1
                counted.append((l1, l2))

            for i, (l1, l2) in enumerate(counted):
                if l1 is None:
                    continue
                for j, (l3, l4) in enumerate(counted[:i]):
                    if l3 is None:
                        continue
                    if l1 <= l3 <= l4 <= l2:
                        counted[j] = None, None

        tot = 0

        if row % 10000 == 0:
            print(row)
        for l1, l2 in counted:
            if l1 is None:
                continue
            l1 = max(l1, 0)
            l2 = min(l2, 4000000)
            if l1 <= l2:
                tot += l2 - l1 + 1
        if tot != 4000001:
            print(row, tot)




def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        syote = syote.replace("Sensor at x=", "").replace(", y=", " ").replace(": closest beacon is at x=", " ").replace(", y=", " ")
        nums = syote.split(" ")
        for i in range(4):
            nums[i] = int(nums[i])
        lista.append(nums)
    return lista


if __name__ == "__main__":
    main()
