import numpy as np


def main():
    lista = get_input()
    print(lista)
    rocks = [((0, 0), (1, 0), (2, 0), (3, 0)),
             ((1, 0), (0, 1), (1, 1), (2, 1), (1, 2)),
             ((2, 2), (2, 1), (0, 0), (1, 0), (2, 0)),
             ((0, 0), (0, 1), (0, 2), (0, 3)),
             ((0, 0), (1, 0), (0, 1), (1, 1))]

    rock_set = set()
    for i in range(7):
        rock_set.add((i, 0))
    rock_i = 0
    push_i = 0
    chamb_width = 7
    highest = 0
    for _ in range(2022):
        rock = rocks[rock_i]
        coords = list()
        for x, y in rock:
            coords.append((x + 2, highest + y + 4))
        rock_i = (rock_i + 1) % len(rocks)
        while True:
            push = lista[push_i]
            push_i = (push_i + 1) % len(lista)
            old_coords = list(coords)
            if push == "<":
                dx = -1
            else:
                dx = 1
            for i in range(len(coords)):
                x, y = coords[i]
                nc = (x + dx, y)
                if nc in rock_set or not 0 <= nc[0] < chamb_width:
                    coords = old_coords
                    break
                coords[i] = nc
            old_coords = list(coords)
            allakivi = False
            for i in range(len(coords)):
                x, y = coords[i]
                nc = (x, y - 1)
                if nc in rock_set:
                    coords = old_coords
                    allakivi = True
                    break
                coords[i] = nc
            if allakivi:
                for x, y in coords:
                    rock_set.add((x, y))
                    if y > highest:
                        highest = y
                break
        # print(rock_set)
        # input("jatka")

    print(highest)
    for y in range(highest):
        y = highest - y - 1
        for x in range(chamb_width):
            if (x, y) in rock_set:
                print("#", end="")
            else:
                print(".", end="")
        print()


def main2():
    lista = get_input()
    print(lista)
    rocks = [((0, 0), (1, 0), (2, 0), (3, 0)),
             ((1, 0), (0, 1), (1, 1), (2, 1), (1, 2)),
             ((2, 2), (2, 1), (0, 0), (1, 0), (2, 0)),
             ((0, 0), (0, 1), (0, 2), (0, 3)),
             ((0, 0), (1, 0), (0, 1), (1, 1))]

    rock_set = set()
    for i in range(7):
        rock_set.add((i, 0))
    rock_i = 0
    push_i = 0
    chamb_width = 7
    highest = 0
    rem = (1000000000000 - 1698) % 1715
    kokblok = (1000000000000 - 1698) // 1715 * 2677

    for rock_num in range(1698 + rem):
        rock = rocks[rock_i]
        coords = list()
        for x, y in rock:
            coords.append((x + 2, highest + y + 4))
        rock_i = (rock_i + 1) % len(rocks)
        while True:
            push = lista[push_i]
            push_i = (push_i + 1) % len(lista)
            old_coords = list(coords)
            if push == "<":
                dx = -1
            else:
                dx = 1
            for i in range(len(coords)):
                x, y = coords[i]
                nc = (x + dx, y)
                if nc in rock_set or not 0 <= nc[0] < chamb_width:
                    coords = old_coords
                    break
                coords[i] = nc
            old_coords = list(coords)
            allakivi = False
            for i in range(len(coords)):
                x, y = coords[i]
                nc = (x, y - 1)
                if nc in rock_set:
                    coords = old_coords
                    allakivi = True
                    break
                coords[i] = nc
            if allakivi:
                for x, y in coords:
                    rock_set.add((x, y))
                    if y > highest:
                        highest = y
                break
        if push_i < 10:
            print(push_i, highest, rock_num)
        # print(rock_set)
        # input("jatka")

    print(highest + kokblok)



def get_input():
    lista = []
    print("Anna syöte:", end="")
    return input()


if __name__ == "__main__":
    main2()
