import numpy as np


def main():
    cmds = get_input()
    dir = dict()
    cur_dir = ""
    dir[""] = list()
    for cmd in cmds[1:]:
        if cmd[0] == "$":
            if cmd[2:4] == "cd":
                newdir = cmd.rsplit(" ", 1)[-1]
                if newdir == "..":
                    cur_dir = cur_dir.rsplit("/", 1)[0]
                else:
                    cur_dir = cur_dir + "/" + newdir
                    if cur_dir not in dir:
                        dir[cur_dir] = list()
        else:
            if cmd.split(" ")[0] == "dir":
                dir[cur_dir].append(cur_dir + "/" + cmd.split(" ", 1)[1])
            else:
                dir[cur_dir].append(int(cmd.split(" ", 1)[0]))

    print(dir)

    dirsizes = dict()

    while True:
        allfound = True
        for dirname in dir:
            dirsize = 0
            countable = True
            for size in dir[dirname]:
                if isinstance(size, int):
                    dirsize += size
                else:
                    # print(dirname, dir[dirname], allfound)
                    if size in dirsizes:
                        dirsize += dirsizes[size]
                    else:
                        countable = False
                        break
            if countable:
                dirsizes[dirname] = dirsize
            else:
                allfound = False

        if allfound:
            break
        for name in dir:
            if name not in dirsizes:
                print(name, dir[name])
        print(len(dirsizes))
        # input("ok")

    tot = 0
    for dirname in dirsizes:
        if dirsizes[dirname] < 100000:
            tot += dirsizes[dirname]
    print(tot)

    raja = 30000000 - (70000000 - dirsizes[""])
    mindir = np.inf
    for dirname in dirsizes:
        sz = dirsizes[dirname]
        if sz >= raja:
            mindir = min(mindir, sz)
    print(mindir)





def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
           break
        lista.append(syote)
    return lista


if __name__ == "__main__":
    main()
