import numpy as np


def main():
    lista = get_input()
    print(lista)

    tot = 0

    for i in range(len(lista)):
        tot += 6
        x, y, z = lista[i]
        for j in range(i+1, len(lista)):
            x2, y2, z2 = lista[j]
            if (abs(x2 -x) < 2 and y2==y and z2 == z) or (abs(z2 -z) < 2 and y2==y and x2 == x) or (abs(y2 -y) < 2 and x2==x and z2 == z):
                tot -= 2
    print(tot)


def main2():
    lista = get_input()
    print(lista)

    minx = 100
    miny = 100
    minz = 100
    maxx = 0
    maxy = 0
    maxz = 0
    for x, y, z in lista:
        maxx = max(maxx, x)
        maxy = max(maxy, y)
        maxz = max(maxz, z)
        minx = min(minx, x)
        miny = min(miny, y)
        minz = min(minz, z)

    print(maxx, maxy, maxz, minx, miny, minz)
    cube = np.zeros((23, 23, 23), dtype=int)
    for x, y, z in lista:
        cube[x, y, z] = 1

    for xl in range(23):
        for yl in range(23):
            for zl in range(23):
                if cube[xl, yl, zl] == 1:
                    continue
                checklist = {(xl, yl, zl)}
                checked = set()
                wallfound = False
                while len(checklist) > 0:
                    x, y, z = checklist.pop()
                    checked.add((x, y, z))
                    if -1 in (x, y, z) or 22 in (x, y, z) or cube[x, y, z] == -1:
                        wallfound = True
                        break
                    for dx, dy, dz in ((1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0), (0, 0, 1), (0, 0, -1)):
                        nc = (x + dx, y + dy, z + dz)
                        if nc in checked or cube[nc[0], nc[1], nc[2]] == 1:
                            continue
                        checklist.add(nc)
                if wallfound:
                    for x, y, z in checked:
                        cube[x, y, z] = -1
                else:
                    for x, y, z in checked:
                        cube[x, y, z] = 1

    tot = 0
    for x, y, z in lista:
        for dx, dy, dz in ((1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0), (0, 0, 1), (0, 0, -1)):
            nc = (x + dx, y + dy, z + dz)
            if cube[nc[0], nc[1], nc[2]] < 1:
                tot += 1
    print(tot)
    print(np.argwhere(cube==0))


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote.split(","))
        for i in range(3):
            lista[-1][i] = int(lista[-1][i])
    return lista


if __name__ == "__main__":
    main2()

"""

"""