import numpy as np


def main():
    lista = get_input()
    print(lista)

    opdict = {"+": plus, "-": minus, "*": mult, "/": div}

    yelllist = ["root"]
    while len(yelllist) > 0:
        name = yelllist[-1]
        if isinstance(lista[name], int):
            yelllist.pop()
            continue
        print(name, lista[name])
        n1, op, n2 = lista[name]
        if isinstance(lista[n1], int) and isinstance(lista[n2], int):
            lista[name] = opdict[op](lista[n1], lista[n2])
            yelllist.pop()
            continue
        if not isinstance(lista[n1], int):
            yelllist.append(n1)
        if not isinstance(lista[n2], int):
            yelllist.append(n2)
    print(lista["root"])


def main2():
    orig_lista = get_input()
    orig_lista["root"][1] = "="
    opdict = {"+": plus, "-": minus, "*": mult, "/": div, "=": equal}
    comp1, comp2 = orig_lista["root"][0], orig_lista["root"][2]
    maxnum = 10000000000000
    minnum = 0
    while True:
        num = 3916936880448 #  (maxnum + minnum) // 2
        lista = dict(orig_lista)
        lista["humn"] = num
        yelllist = ["root"]
        while len(yelllist) > 0:
            name = yelllist[-1]
            if isinstance(lista[name], int):
                yelllist.pop()
                continue
            # print(name, lista[name])
            n1, op, n2 = lista[name]
            if isinstance(lista[n1], int) and isinstance(lista[n2], int):
                lista[name] = opdict[op](lista[n1], lista[n2])
                yelllist.pop()
                continue
            if not isinstance(lista[n1], int):
                yelllist.append(n1)
            if not isinstance(lista[n2], int):
                yelllist.append(n2)
        print(num, minnum, maxnum, lista[comp1], lista[comp2], lista[comp1] > lista[comp2])
        if lista[comp1] > lista[comp2]:
            minnum = num
        elif lista[comp1] < lista[comp2]:
            maxnum = num
        else:
            print("Oikea", num)
            break



def get_input():
    lista = dict()
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        name, rest = syote.split(":")
        tied = rest.strip().split(" ")
        if len(tied) == 1:
            lista[name] = int(tied[0])
        else:
            r1, op, r2 = tied
            lista[name] = [r1, op, r2]
    return lista


def plus(a1, a2):
    return a1 + a2


def minus(a1, a2):
    return a1 - a2


def mult(a1, a2):
    return a1 * a2


def div(a1, a2):
    return a1 // a2


def equal(a1, a2):
    return a1 == a2

if __name__ == "__main__":
    main2()
