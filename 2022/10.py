import numpy as np


def main():
    lista = get_input()
    print(lista)

    tot = 0
    reg = 1
    cycle = 0
    clim = 20
    for s in lista:
        if s == "noop":
            cycle += 1
        else:
            cycle += 2
        if cycle >= clim:
            tot += reg * clim
            print(reg, clim, reg*clim)
            clim += 40
            if clim > 220:
                pass
        if s != "noop":
            reg += int(s.split()[1])

    print(tot)

    ctr = [[None]*40 for _ in range(6)]
    i = 0
    reg = 1
    for s in lista:
        x, y = i % 40, i // 40
        if abs(x - reg) < 2:
            ctr[y][x] = "#"
        else:
            ctr[y][x] = "."
        i += 1
        if s == "noop":
            pass
        else:
            x, y = i % 40, i // 40
            if abs(x - reg) < 2:
                ctr[y][x] = "#"
            else:
                ctr[y][x] = "."
            i += 1
            reg += int(s.split()[1])


    for line in ctr:
        print("".join(line))



def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote)
    return lista


if __name__ == "__main__":
    main()
