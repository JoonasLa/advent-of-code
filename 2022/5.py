import numpy as np


def main():
    lis, lista = get_input()
    print(lis, lista)

    for maar, f, to in lista:
        for _ in range(maar):
            lis[to-1] = [lis[f-1][0]] + lis[to-1]
            lis[f-1] = lis[f-1][1:]
        print(lis)

    for i in range(9):
        print(lis[i][0], end="")

    lis, lista = get_input()
    print(lis, lista)

    for maar, f, to in lista:
        lis[to - 1] = lis[f - 1][:maar] + lis[to - 1]
        lis[f - 1] = lis[f - 1][maar:]
        print(lis)

    for i in range(9):
        print(lis[i][0], end="")

def get_input():
    lista = []
    print("Anna syöte:", end="")
    koko = 9
    ser = 0
    lis = [list() for x in range(koko)]
    while True:
        syote = input()
        if syote == "":
            ser += 1
            if ser == 2:
                break
        elif ser == 0:
            for i in range(koko):
                if syote[i*4 + 1] != " ":
                    lis[i].append(syote[i*4 + 1])

        elif ser == 1:
            _, n1, _, n2, _, n3 = syote.split(" ")
            lista.append((int(n1), int(n2), int(n3)))
    for i in range(koko):
        lis[i].pop()
    return lis, lista


if __name__ == "__main__":
    main()
