import numpy as np


def main():
    lista = get_input()
    print(lista)

    tot = 0
    for s in lista:
        cut = len(s) // 2
        s1, s2 = s[:cut], s[cut:]
        for l in s1:
            if l in s2:
                break

        k = ord(l)
        if k >= 97:
            lis =  k - 96
        else:
            lis =  k - 64 + 26
        tot += lis
        print(l, lis)
    print(tot)

    tot = 0
    for i in range(0, len(lista), 3):
        s1, s2, s3 = lista[i], lista[i+1], lista[i+2]
        for l in s1:
            if l in s2 and l in s3:
                break
        k = ord(l)
        if k >= 97:
            lis = k - 96
        else:
            lis = k - 64 + 26
        tot += lis
        print(l, lis)
    print(tot)
def get_input():
    lista = []

    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote)
    return lista


if __name__ == "__main__":
    main()
