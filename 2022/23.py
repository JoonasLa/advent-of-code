import numpy as np


def main():
    lista = get_input()
    print(lista)

    elfcoord = set()
    for y in range(len(lista)):
        for x in range(len(lista[0])):
            if lista[y][x] == "#":
                elfcoord.add((x, -y))
    print(len(elfcoord))
    movelist = ((0, 1), (0, -1), (-1, 0), (1, 0))
    for step in range(100000):
        anymove = False
        moves = dict()
        for x, y in elfcoord:
            elffound = False
            for dy in range(-1, 2):
                for dx in range(-1, 2):
                    if dy == dx == 0:
                        continue
                    if (x + dx, y + dy) in elfcoord:
                        elffound = True
                        break
                if elffound:
                    break

            if not elffound:
                continue
            for dx, dy in movelist:
                xx, yy = x + dx, y + dy
                if dx == 0:
                    dl2 = ((0, 0), (1, 0), (-1, 0))
                else:
                    dl2 = ((0, 0), (0, 1), (0, -1))
                for ddx, ddy in dl2:
                    xxx, yyy = xx + ddx, yy + ddy
                    if (xxx, yyy) in elfcoord:
                        break
                else:
                    if (xx, yy) not in moves:
                        moves[(xx, yy)] = list()
                    moves[(xx, yy)].append((x, y))
                    break
        for move in moves:
            if len(moves[move]) == 1:
                anymove = True
                elfcoord.add(move)
                elfcoord.remove(moves[move][0])
        if not anymove:
            print(step+1, "Ei enää liikkunut")
            break
        movelist = movelist[1:] + (movelist[0],)
        print(step)
        # print_elfcoord(elfcoord)

    minx, miny, maxx, maxy = np.inf, np.inf, -np.inf, -np.inf
    for x, y in elfcoord:
        minx = min(minx, x)
        maxx = max(maxx, x)
        miny = min(miny, y)
        maxy = max(maxy, y)

    print((maxx-minx + 1) * (maxy - miny + 1) - len(elfcoord))
    print(len(elfcoord))
    print(maxx, minx, maxy, miny)
    print(len(lista[0]), len(lista))
    print(movelist)


def print_elfcoord(elfcoord):
    minx, miny, maxx, maxy = np.inf, np.inf, -np.inf, -np.inf
    for x, y in elfcoord:
        minx = min(minx, x)
        maxx = max(maxx, x)
        miny = min(miny, y)
        maxy = max(maxy, y)
    for y in range(miny, maxy+1):
        for x in range(minx, maxx + 1):
            if (x, y) in elfcoord:
                print("#", end="")
            else:
                print(".", end="")
        print()


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote)
    return lista


if __name__ == "__main__":
    main()
