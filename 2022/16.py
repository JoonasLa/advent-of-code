import numpy as np
import heapq
from numba import njit

def main():
    lista = get_input()
    print(lista)
    maxfrate = 0
    notzero = 0
    maxflist = list()
    for name in lista:
        frate = lista[name][0]
        if frate > 0:
            maxflist.append((frate, name))
        maxfrate += frate
    maxflist = sorted(maxflist, reverse=True)

    heap = list()
    heap.append((-np.inf, 0, 30, "AA", set()))
    print(heap)
    minmaxtot = -np.inf
    while True:
        maxtot, tot, minleft, curvalv, openvalv = heapq.heappop(heap)
        if maxtot > minmaxtot:
            print(maxtot, minleft)
            minmaxtot = maxtot
        if minleft == 0:
            print(tot)
            print(maxtot, minleft, curvalv, openvalv)
            break
        ml2 = minleft-1
        maxadd = 0
        md2 = 0

        for ovalv in openvalv:
            md2 += lista[ovalv][0]
        for fadd, n2 in maxflist:
            if n2 in openvalv:
                continue
            maxadd += md2
            ml2 -= 1
            if ml2 <= 0:
                break
            md2 += fadd
            ml2 -= 1
            maxadd += md2
            if ml2 <= 0:
                break
        maxadd += md2 * ml2

        curflow = 0
        minleft -= 1
        for n in openvalv:
            curflow += lista[n][0]

        tot += curflow
        o2 = set(openvalv)
        o2.add(curvalv)
        heapq.heappush(heap, (-maxadd - tot, tot, minleft, curvalv, o2))

        for nvalv in lista[curvalv][1]:
            o2 = set(openvalv)
            heapq.heappush(heap, (-maxadd - tot, tot, minleft, nvalv, o2))
    print(heap[:10])


def main2():
    lista = get_input()
    print(lista)
    maxfrate = 0
    notzero = 0
    maxflist = list()
    for name in lista:
        frate = lista[name][0]
        if frate > 0:
            maxflist.append((frate, name))
        maxfrate += frate
    maxflist = sorted(maxflist, reverse=True)

    distances = count_distances(lista)
    print(distances)


    heap = list()
    heap.append((-np.inf, 0, 26, "AA", "AA", set()))
    print(heap)
    minmaxtot = -np.inf
    while True:
        maxtot, tot, minleft, curvalv, elvalv, openvalv = heapq.heappop(heap)
        if maxtot > minmaxtot:
            print(maxtot, minleft)
            minmaxtot = maxtot
        # if maxtot < minmaxtot:
            # print("Jokin mennee vikaan")
        if minleft == 0:
            print(tot)
            print(maxtot, minleft, curvalv, openvalv)
            break

        curflow = 0
        minleft -= 1
        for n in openvalv:
            curflow += lista[n][0]
        tot += curflow

        if len(openvalv) == len(maxflist):
            heapq.heappush(heap, (-tot - curflow * minleft, tot + curflow * minleft, 0, curvalv, elvalv, openvalv))
            continue
        if curvalv not in openvalv and elvalv not in openvalv:
            o2 = set(openvalv)
            o2.add(curvalv)
            o2.add(elvalv)
            maxadd = count_max_addition(curvalv, elvalv, curflow, o2, minleft, maxflist, distances, lista)
            heapq.heappush(heap, (-maxadd - tot, tot, minleft, curvalv, elvalv, o2))
        skipnum = 30
        if curvalv not in openvalv:
            for nvalv in lista[elvalv][1]:
                if nvalv == curvalv or (elvalv not in openvalv and lista[elvalv][0] >= skipnum):
                    continue
                o2 = set(openvalv)
                o2.add(curvalv)
                maxadd = count_max_addition(curvalv, nvalv, curflow, o2, minleft, maxflist, distances, lista)
                heapq.heappush(heap, (-maxadd - tot, tot, minleft, curvalv, nvalv, o2))

        for nvalv in lista[curvalv][1]:
            if curvalv not in openvalv and lista[curvalv][0] >= skipnum:
                continue
            if elvalv not in openvalv:
                o2 = set(openvalv)
                o2.add(elvalv)
                maxadd = count_max_addition(nvalv, elvalv, curflow, o2, minleft, maxflist, distances, lista)
                heapq.heappush(heap, (-maxadd - tot, tot, minleft, nvalv, elvalv, o2))
            for nvalv2 in lista[elvalv][1]:
                if elvalv not in openvalv and lista[elvalv][0] >= skipnum:
                    continue
                if nvalv == nvalv2:
                    continue
                o2 = set(openvalv)
                maxadd = count_max_addition(nvalv, nvalv2, curflow, o2, minleft, maxflist, distances, lista)
                heapq.heappush(heap, (-maxadd - tot, tot, minleft, nvalv, nvalv2, o2))
    print(heap[:10])


def main3():
    lista = get_input()
    print(lista)
    locs = ["AA", "AA"]
    dests = [None, None]
    distances = count_distances(lista)
    print(distances)
    open_valves = set()
    tot = 0
    for minute in range(26):
        minutes_left = 26 - minute

        add_tot = 0
        for ov in open_valves:
            add_tot += lista[ov][0]
        tot += add_tot

        for i in range(2):
            loc = locs[i]
            dest = dests[i]
            if dest is None:
                max_add = 0
                max_dest = None
                for v in lista:
                    if v not in open_valves and v != dests[(i+1) % 2]:
                        add = lista[v][0] * (minutes_left - distances[loc][v][0] - 1)
                        ml2 = minutes_left - distances[loc][v][0] - 1
                        print(loc, v, add)
                        if add > max_add:
                            max_add = add
                            max_dest = v
                dests[i] = max_dest
                dest = max_dest
                # print(i, "Kannattaa mennä", dest, max_add)
            if dest is None:
                pass
            elif loc == dest:
                dests[i] = None
                open_valves.add(loc)
            else:
                locs[i] = distances[loc][dest][1]

        print(f"Minuutti {minute:2d} lisäys {add_tot:3d} Yhteensä {tot:4d}", locs, dests, open_valves)


def main4():
    lista = get_input()
    print(lista)
    distances = count_distances(lista)
    print(distances)
    cons_valves = set()
    for vv in lista:
        if lista[vv][0] > 0:
            cons_valves.add(vv)

    heap = [(0, [], [])]
    maxtot = 0
    while True:
        tot, l1, l2 = heapq.heappop(heap)
        tot *= -1
        if tot > maxtot:
            maxtot = tot
            print("Uusi maksimi:", maxtot, l1, l2)
        # print(tot, len(heap), l1, l2)

        for vv in cons_valves:
            if vv not in l1 and vv not in l2:
                b1 = count_plan_tot(l1 + [vv], lista, distances) + count_plan_tot(l2, lista, distances)
                if not np.isnan(b1):
                    heapq.heappush(heap, (-b1, l1 + [vv], l2))
                if l1 != l2:
                    b1 = count_plan_tot(l1, lista, distances) + count_plan_tot(l2 + [vv], lista, distances)
                    if not np.isnan(b1):
                        heapq.heappush(heap, (-b1, l1, l2 + [vv]))


def main5():
    lista = get_input()
    print(lista)
    distances = count_distances(lista)
    print(distances)
    cons_valves = set()
    for vv in lista:
        if lista[vv][0] > 0:
            cons_valves.add(vv)

    heap = [(0, [], [])]
    maxtot = 0
    while True:
        tot, l1, l2 = heapq.heappop(heap)
        tot *= -1
        if tot > maxtot:
            maxtot = tot
            print("Uusi maksimi:", maxtot, l1, l2)
        # print(tot, len(heap), l1, l2)

        free_valves = set(cons_valves)
        for vv in l1 + l2:
            free_valves.remove(vv)

        addlist = create_addlists(free_valves)
        b2 = count_plan_tot(l2, lista, distances)
        for add in addlist:
            ll1 = l1 + add
            b1 = count_plan_tot(ll1, lista, distances)
            if not np.isnan(b1):
                heapq.heappush(heap, (-b1 - b2, ll1, l2))
        if l1 != l2:
            b1 = count_plan_tot(l1, lista, distances)
            for add in addlist:
                ll2 = l2 + add
                b2 = count_plan_tot(ll2, lista, distances)
                if not np.isnan(b2):
                    heapq.heappush(heap, (-b1 - b2, l1, ll2))

addlist_helper = dict()
def create_addlists(free_valves):
    if frozenset(free_valves) in addlist_helper:
        return addlist_helper[frozenset(free_valves)]
    addlist = list()
    for vv in free_valves:
        addlist.append([vv])

    start, stop = 0, len(addlist)
    for i in range(start, stop):
        vl = addlist[i]
        for vv in free_valves:
            if vv not in vl:
                addlist.append(vl + [vv])

    for _ in range(2, 9):
        start, stop = stop, len(addlist)
        for i in range(start, stop):
            vl = addlist[i]
            for vv in free_valves:
                if vv not in vl:
                    addlist.append(vl + [vv])

    addlist_helper[frozenset(free_valves)] = addlist
    return addlist


def count_plan_tot(plan, lista, distances):
    minleft = 26
    loc = "AA"
    tot = 0
    for vv in plan:
        minleft -= distances[loc][vv][0]+1
        if minleft <= 0:
            return np.nan
        tot += lista[vv][0] * minleft
        loc = vv
    return tot


def count_max_addition(curvalv, elvalv, curflow, openvalv, minleft, maxflist, distances, lista):
    ml2 = minleft
    maxadd = 0
    md2 = 0
    maxflist = list(maxflist)
    for v in openvalv:
        md2 += lista[v][0]
    for i in range(len(maxflist)):
        if maxflist[i][1] in openvalv:
            maxflist[i] = (None, None)

    roomi = 0
    while ml2 > 0:
        maxadd += md2
        ml2 -= 1
        if ml2 == 0:
            break
        numadd = 0
        for i, (nf, n2) in enumerate(maxflist):
            if n2 is None:
                continue
            if distances[curvalv][n2] > roomi and distances[elvalv][n2] > roomi:
                continue

            md2 += nf
            maxflist[i] = (None, None)
            numadd += 1
            if numadd == 2:
                break
        roomi += 1

        """maxadd += md2
        ml2 -= 1
        if ml2 <= 0:
            break"""
    return maxadd


def count_distances(lista):
    rooms = set(lista.keys())
    dists = dict()
    for room in rooms:
        dists[room] = dict()
        for r2 in rooms:
            dists[room][r2] = (np.inf, None)
        for r2 in lista[room][1]:
            dists[room][r2] = (1, r2)
        dists[room][room] = (0, None)

    while len(rooms) > 0:
        room = rooms.pop()
        for r2 in lista[room][1]:
            for r3 in dists[r2]:
                d2r = dists[r2][r3][0] + 1
                if d2r < dists[room][r3][0]:
                    dists[room][r3] = (d2r, r2)
                    rooms.add(room)
    return dists


def get_input():
    lista = dict()
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        syote = syote.replace("Valve ", "").replace("s", "").replace(" ha flow rate=", " ").replace("; tunnel lead to valve ", " ").replace(",", "")
        tiedot = syote.split(" ")

        lista[tiedot[0]] = (int(tiedot[1]), tiedot[2:])
    return lista


if __name__ == "__main__":
    main5()

# 2963 < vastaus < 3138
#  ['DY', 'XJ', 'RC', 'HF', 'CF', 'VG'] ['DP', 'RU', 'KG', 'EF', 'AH', 'XE'] 2963