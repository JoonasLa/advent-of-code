import numpy as np


def main():
    lista = get_input()
    print(lista)
    print(len(lista))
    tot = 0
    for i, (l1, l2) in enumerate(lista):
        comp = compare(l1, l2)
        print(l1)
        print(l2)
        print(comp)
        if comp == -1:
            tot += i+1
        if comp == 0:
            input("stop")

    print(tot)

    l2 = list()
    for lr, rr in lista:
        l2.append(lr)
        l2.append(rr)
    lista = l2

    left = [[2]]
    nleft = 1
    for right in lista:
        if compare(left, right) == +1:
            nleft += 1

    left = [[6]]
    nright = 2
    for right in lista:
        if compare(left, right) == +1:
            nright += 1
    print(nleft, nright)
    print(nleft * nright)



def compare(left, right):
    if isinstance(left, int):
        if isinstance(right, int):
            if left < right:
                return -1
            elif left > right:
                return +1
            return 0
        else:
            return compare([left], right)
    else:
        if isinstance(right, int):
            return compare(left, [right])
        else:
            for k in range(len(left)):
                if k == len(right):
                    return 1
                val = compare(left[k], right[k])
                if val != 0:
                    return val
            if len(left) == len(right):
                return 0
            return -1


def get_input():
    lista = []
    print("Anna syöte:", end="")
    tyhja = 0
    lis = list()
    while True:
        syote = input()
        if syote == "":
            if len(lis) == 2:
                lista.append(lis)
            lis = list()
            tyhja += 1
            if tyhja == 2:
                break
        else:
            lis.append(eval(syote))
            tyhja = 0

    return lista


if __name__ == "__main__":
    main()
