import numpy as np


def main():
    lista, ohjeet = get_input()
    """print(lista)
    print(ohjeet)
    x, y = 0, 0
    lev, height = len(lista[0]), len(lista)
    while lista[y][x] == " ":
        x += 1
    rot = 0
    dirs = ((1, 0), (0, 1), (-1, 0), (0, -1))
    for oh in ohjeet:
        if oh == "R":
            rot = (rot + 1) % 4
        elif oh == "L":
            rot = (rot - 1) % 4
        else:
            dx, dy = dirs[rot]
            for _ in range(oh):
                xv, yv = x, y
                x, y = (x + dx) % lev, (y + dy) % height
                while lista[y][x] == " ":
                    x, y = (x + dx) % lev, (y + dy) % height
                if lista[y][x] == "#":
                    x, y = xv, yv
                    break

    print(y*1000+1000+x*4+4+rot)"""


    x, y = 0, 0
    lev, height = len(lista[0]), len(lista)
    while lista[y][x] == " ":
        x += 1
    rot = 0
    dirs = ((1, 0), (0, 1), (-1, 0), (0, -1))
    alue = 1

    hypyt = {(1, 0):(2, 0),
             (1, 1):(3, 1),
             (1, 2):(4, 0),
             (1, 3):(6, 0),
             (2, 0):(5, 2),
             (2, 1):(3, 2),
             (2, 2):(1, 2),
             (2, 3):(6, 3),
             (3, 0):(2, 3),
             (3, 1):(5, 1),
             (3, 2):(4, 1),
             (3, 3):(1, 3),
             (4, 0):(5, 0),
             (4, 1):(6, 1),
             (4, 2):(1, 0),
             (4, 3):(3, 0),
             (5, 0):(2, 2),
             (5, 1):(6, 2),
             (5, 2):(4, 2),
             (5, 3):(3, 3),
             (6, 0):(5, 3),
             (6, 1):(2, 1),
             (6, 2):(1, 1),
             (6, 3):(4, 3)}
    alkulmat = {1:(50, 0), 2:(100, 0), 3:(50, 50), 4:(0, 100), 5:(50, 100), 6:(0, 150)}

    for oh in ohjeet:
        if oh == "R":
            rot = (rot + 1) % 4
        elif oh == "L":
            rot = (rot - 1) % 4
        else:
            for _ in range(oh):
                dx, dy = dirs[rot]
                xv, yv, rotv, aluev = x, y, rot, alue
                x, y = x + dx, y + dy

                siir = None
                if rot == 0 and x % 50 == 0:
                    siir = y % 50
                if rot == 1 and y % 50 == 0:
                    siir = x % 50
                if rot == 2 and x % 50 == 49:
                    siir = y % 50
                if rot == 3 and y % 50 == 49:
                    siir = x % 50

                if siir is not None:
                    nalue, nrot = hypyt[(alue, rot)]
                    if abs(nrot - rot) == 2:
                        siir = 49 - siir
                    xx, yy = alkulmat[nalue]
                    if nrot == 0:
                        x, y = xx, yy + siir
                    if nrot == 1:
                        x, y = xx + siir, yy
                    if nrot == 2:
                        x, y = xx + 49, yy + siir
                    if nrot == 3:
                        x, y = xx + siir, yy + 49
                    rot = nrot
                    alue = nalue
                    print(xv, yv, rotv, aluev, "->", x, y, rot, alue)

                if lista[y][x] == "#":
                    x, y, rot, alue = xv, yv, rotv, aluev
                    break

        # print(oh, x, y, rot)

    print(y * 1000 + 1000 + x * 4 + 4 + rot)


def get_input():
    lista = []
    print("Anna syöte:", end="")
    lev = None
    while True:
        syote = input()
        if syote == "":
            break
        if lev is None:
            lev = len(syote)
        lista.append(syote + " "*(lev - len(syote)))
    syote = input("")
    ohjeet = list()
    j = 0
    for i in range(len(syote)):
        if syote[i] in ("L", 'R'):
            ohjeet.append(int(syote[j:i]))
            ohjeet.append(syote[i])
            j = i + 1
    ohjeet.append(int(syote[j:]))
    return lista, ohjeet


if __name__ == "__main__":
    main()
