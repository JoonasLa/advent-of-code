import numpy as np


def main():
    lista = get_input()
    print(lista)

    visited = {(0, 0)}
    hx, hy = 0, 0
    tx, ty = 0, 0
    sdict = {"U":(0, 1), "D":(0, -1), "L": (-1, 0), "R": (1, 0)}
    for suunta, matka in lista:
        for _ in range(matka):
            hx += sdict[suunta][0]
            hy += sdict[suunta][1]
            if abs(hx - tx) > 1 or abs(hy - ty) > 1:
                tx, ty = hx, hy
                tx -= sdict[suunta][0]
                ty -= sdict[suunta][1]
            visited.add((tx, ty))
    print(len(visited))

    visited = [(0, 0)]
    coords = [[0, 0] for _ in range(10)]
    print(coords)
    sdict = {"U": (0, 1), "D": (0, -1), "L": (-1, 0), "R": (1, 0)}
    for suunta, matka in lista:
        for _ in range(matka):
            coords[0][0] += sdict[suunta][0]
            coords[0][1] += sdict[suunta][1]
            for i in range(1, len(coords)):
                if abs(coords[i][0] - coords[i-1][0]) > 1 or abs(coords[i][1] - coords[i-1][1]) > 1:
                    sx, sy = 0, 0
                    if coords[i-1][1] > coords[i][1] + 1:
                        sy = -1
                    if coords[i-1][1] < coords[i][1] - 1:
                        sy = 1
                    if coords[i-1][0] > coords[i][0] + 1:
                        sx = -1
                    if coords[i-1][0] < coords[i][0] - 1:
                        sx = 1
                    coords[i][0] = coords[i-1][0] + sx
                    coords[i][1] = coords[i-1][1] + sy
            for i in range(1, len(coords)):
                assert abs(coords[i][0] - coords[i-1][0]) <= 1 and abs(coords[i][1] - coords[i-1][1]) <= 1, "VIRHE"

            tc = tuple(coords[-1])
            if tc not in visited:
                visited.append(tc)
            # visited.add(tuple(coords[-1]))
            # print(coords)
    print(visited)
    print(len(visited))


def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote.split(" "))
        lista[-1][1] = int(lista[-1][1])
    return lista


if __name__ == "__main__":
    main()
