import numpy as np


def main():
    lista = get_input()
    print(lista)
    nums = {"X": 1, "Y": 2, "Z": 3}
    p = 0
    for a, b in lista:
        p += nums[b]
        if a == "A":
            if b == "Y":
                p += 6
            if b == "X":
                p += 3
        if a == "B":
            if b == "Z":
                p += 6
            if b == "Y":
                p += 3
        if a == "C":
            if b == "X":
                p += 6
            if b == "Z":
                p += 3
    print(p)

    p = 0
    for a, b in lista:
        if a == "A":
            if b == "Z":
                b = "Y"
            elif b == "Y":
                b = "X"
            elif b == "X":
                b = "Z"
        if a == "B":
            if b == "Z":
                b = "Z"
            elif b == "Y":
                b = "Y"
            elif b == "X":
                b = "X"
        if a == "C":
            if b == "Z":
                b = "X"
            elif b == "Y":
                b = "Z"
            elif b == "X":
                b = "Y"

        print(a, b)
        p += nums[b]
        if a == "A":
            if b == "Y":
                p += 6
            if b == "X":
                p += 3
        if a == "B":
            if b == "Z":
                p += 6
            if b == "Y":
                p += 3
        if a == "C":
            if b == "X":
                p += 6
            if b == "Z":
                p += 3
    print(p)

def get_input():
    lista = []

    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(syote.split())
    return lista


if __name__ == "__main__":
    main()
