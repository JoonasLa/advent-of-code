import numpy as np
import heapq
from numba import njit, uint8, int8, int32
from numba.types import UniTuple
import numba
import typing

def main():
    lista = get_input()
    print(lista)

    qualitylvl = 0
    for bp, orerobore, clayrobore, obsrobore, obsrobclay, geoderobore, geoderobobs in lista:
        robs = np.array((1, 0, 0, 0))
        ores = np.array((0, 0, 0, 0))
        orerobcost = np.array((orerobore, 0, 0, 0))
        clayrobcost = np.array((clayrobore, 0, 0, 0))
        obsrobcost = np.array((obsrobore, obsrobclay, 0, 0))
        geoderobcost = np.array((geoderobore, 0, geoderobobs, 0))

        heap = list()
        heap.append((0, 0, 24, 0, robs, ores))
        typeraiter = 0
        while True:
            print(heap[0])
            # input("ok")
            maxtot, tot, minleft, _, robs, ores = heapq.heappop(heap)
            # print(tot)
            if minleft == 0:
                qualitylvl += tot * bp
                print(tot, qualitylvl)
                print(robs, ores)
                break
            minleft -= 1

            # Ei rakenneta
            r2 = np.array(robs)
            o2 = np.array(ores)

            o2 += robs
            maxtot = count_maxtot(tot, minleft, robs, ores)
            typeraiter += 1
            heapq.heappush(heap, (maxtot, o2[3], minleft, typeraiter, r2, o2))

            # Ore robo
            if np.all(ores >= orerobcost):
                r2 = np.array(robs)
                r2[0] += 1
                o2 = np.array(ores)
                o2 += robs
                o2 -= orerobcost
                # maxtot = count_maxtot(tot, minleft, r2, o2)
                typeraiter += 1
                heapq.heappush(heap, (maxtot, o2[3], minleft, typeraiter, r2, o2))

            # Clay robo
            if np.all(ores >= clayrobcost):
                r2 = np.array(robs)
                r2[1] += 1
                o2 = np.array(ores)
                o2 += robs
                o2 -= clayrobcost
                # maxtot = count_maxtot(tot, minleft, r2, o2)
                typeraiter += 1
                heapq.heappush(heap, (maxtot, o2[3], minleft, typeraiter, r2, o2))

            # Obs robo
            if np.all(ores >= obsrobcost):
                r2 = np.array(robs)
                r2[2] += 1
                o2 = np.array(ores)
                o2 += robs
                o2 -= obsrobcost
                # maxtot = count_maxtot(tot, minleft, r2, o2)
                typeraiter += 1
                heapq.heappush(heap, (maxtot, o2[3], minleft, typeraiter, r2, o2))

            # geo robo
            if np.all(ores >= geoderobcost):
                r2 = np.array(robs)
                r2[3] += 1
                o2 = np.array(ores)
                o2 += robs
                o2 -= geoderobcost
                maxtot = count_maxtot(tot, minleft, r2, o2)
                typeraiter += 1
                heapq.heappush(heap, (maxtot, o2[3], minleft, typeraiter, r2, o2))


    print("Maksimi", qualitylvl)


def main2():
    lista = get_input()
    print(lista)

    qualitylvl = 0
    for bp, orerobore, clayrobore, obsrobore, obsrobclay, geoderobore, geoderobobs in lista:
        robs = np.array((1, 0, 0, 0))
        ores = np.array((0, 0, 0, 0))
        orerobcost = np.array((orerobore, 0, 0, 0))
        clayrobcost = np.array((clayrobore, 0, 0, 0))
        obsrobcost = np.array((obsrobore, obsrobclay, 0, 0))
        geoderobcost = np.array((geoderobore, 0, geoderobobs, 0))

        for minute in range(24):
            robs_start = np.array(robs)

            if np.all(ores >= geoderobcost):
                ores -= geoderobcost
                robs[3] += 1
            else:
                if robs[2]/robs[0] < geoderobcost[2]/geoderobcost[0]:
                    # TARVII LIsää obsidiania
                    if np.all(ores >= obsrobcost):
                        ores -= obsrobcost
                        robs[2] += 1
                    else:
                        if robs[1]/robs[0] < obsrobcost[1]/obsrobcost[0]:
                            # Tarvii lisää clayta
                            if np.all(ores >= clayrobcost):
                                ores -= clayrobcost
                                robs[1] += 1
                        else:
                            if np.all(ores >= orerobcost):
                                ores -= orerobcost
                                robs[0] += 1
                else:
                    if np.all(ores >= orerobcost):
                        ores -= orerobcost
                        robs[0] += 1
            ores += robs_start
            print(minute+1, ores, robs)
        print("Valmis", robs, ores)
        qualitylvl += ores[3] * bp
        print(qualitylvl)

    print("Maksimi", qualitylvl)


def main3():
    lista = get_input()
    tot = 1
    for bp, orerobore, clayrobore, obsrobore, obsrobclay, geoderobore, geoderobobs in lista[:3]:
        print("Blueprint:", bp)
        orerob = np.array([orerobore, 0, 0, 0])
        clayrob = np.array([clayrobore, 0, 0, 0])
        obsrob = np.array([obsrobore, obsrobclay, 0, 0])
        geoderob = np.array([geoderobore, 0, geoderobobs, 0])
        heap = [(-2400, tuple())]
        tot *= numba_help(orerob, clayrob, obsrob, geoderob, heap)
    print("Lopullinen tulos:", tot)


# @njit()
def numba_help(orerob, clayrob, obsrob, geoderob, heap):

    maxgeod = 0
    bbb = 2400
    while len(heap) > 0:
        maxbb, plan = heapq.heappop(heap)
        maxbb *= -1
        if maxbb <= maxgeod:
            print("Paras tulos:", maxbb)
            return maxgeod
        if maxbb < bbb:
            print("Maxbb:", maxbb)
            bbb = maxbb
        for i in range(4):
            plan2 = plan + (i,)
            bb, minute_left = count_plan_geodes(plan2, orerob, clayrob, obsrob, geoderob)

            if bb < 0:
                continue
            maxbb = bb + minute_left * (minute_left - 1) // 2

            if bb > maxgeod:
                print("Maxgeod", bb)
                maxgeod = bb
            if maxbb > maxgeod:
                heapq.heappush(heap, (-maxbb, plan2))
    return maxgeod


@njit()
def count_plan_geodes(plan, orerob, clayrob, obsrob, geoderob):
    robs = np.array([1, 0, 0, 0], dtype=int32)
    ores = np.array([0, 0, 0, 0], dtype=int32)
    plani = 0
    rob_i = plan[plani]
    robcosts = [orerob, clayrob, obsrob, geoderob]
    for minute in range(32):
        if np.all(ores >= robcosts[rob_i]):
            robs[rob_i] += 1
            ores -= robcosts[rob_i]
            ores[rob_i] -= 1
            plani += 1
            if plani == len(plan):
                ores += robs
                return ores[-1] + robs[-1] * (31-minute), 31-minute

            rob_i = plan[plani]
        ores += robs
        # print(minute, ores, robs)

    return -1, 0


def count_maxtot(tot, minleft, robs, ores):
    t = robs[3]
    for m in range(minleft):
        tot += t
        t += 1
        tot += np.sum(robs)
    return -tot


def main4():
    lista = get_input()
    tot = 0
    for bp, orerobore, clayrobore, obsrobore, obsrobclay, geoderobore, geoderobobs in lista:
        print("Blueprint:", bp)
        orerob = np.array([orerobore, 0, 0, 0])
        clayrob = np.array([clayrobore, 0, 0, 0])
        obsrob = np.array([obsrobore, obsrobclay, 0, 0])
        geoderob = np.array([geoderobore, 0, geoderobobs, 0])
        maxbb = kokeile_kaikki(orerob, clayrob, obsrob, geoderob)
        tot += bp * maxbb
    print("Lopullinen tulos:", tot)


def kokeile_kaikki(orerob, clayrob, obsrob, geoderob):
    l1 = {tuple()}
    maxbb = 0
    while len(l1) > 0:
        # print(l1)
        l2 = set()
        for l in l1:
            for i in range(4):
                l3 = l + (i,)
                for k in range(len(l3)):
                    l4 = l3[:k] + (1,) + l3[k:]
                    for m in range(k+1, len(l4)):
                        l5 = l4[:m] + (2,) + l4[m:] + (3,)
                        bb, _ = count_plan_geodes(l5, orerob, clayrob, obsrob, geoderob)
                        if bb < 0 and len(l3) > 1:
                            continue
                        l2.add(l3)
                        if bb > maxbb:
                            maxbb = bb
                            print("Uusi paras:", bb, l5)
        l1 = l2
    return maxbb

def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        syote = syote.replace("Blueprint ", "").replace(": Each ore robot costs ", " ").replace(" ore. Each clay robot costs ", " ").replace(" ore. Each obsidian robot costs ", " ").replace(" ore and ", " ").replace(" clay. Each geode robot costs ", " ").replace(" ore and ", " ").replace(" obsidian.", "")
        tiedot = syote.split(" ")
        for i in range(len(tiedot)):
            tiedot[i] = int(tiedot[i])
        lista.append(tiedot)
    return lista


if __name__ == "__main__":
    main3()

"""
Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.
"""
#  VAstaus > 1560
#  VAstaus > 1566
#  VAstaus = 1675