import numpy as np
import heapq

def main():
    lista = get_input()
    print(lista)

    alvaihd = set()

    for i in range(len(lista)):
        for j in range(len(lista[0])):
            if lista[i][j] == "S":
                lista[i][j] = "a"
                alx, ali = j, i
            if lista[i][j] == "E":
                lista[i][j] = "z"
                lox, loy = j, i
            if lista[i][j] == "a":
                alvaihd.add((j, i))
    x, y = alx, ali
    print(alx, ali, lox, loy)
    heap = list()
    heap.append((0, x, y))
    kayty = {(alx, ali)}
    while True:
        matka, x, y = heapq.heappop(heap)
        for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1)):
            xx = x + dx
            yy = y + dy
            if not 0 <= xx < len(lista[0]) or not 0 <= yy < len(lista):
                continue
            if (xx, yy) in kayty:
                continue
            if ord(lista[yy][xx]) - ord(lista[y][x]) <= 1:
                heapq.heappush(heap, (matka + 1, xx, yy))
                kayty.add((xx, yy))
                if (xx, yy) == (lox, loy):
                    print(matka + 1)
                    return

    minpath = np.inf
    for alx, aly in alvaihd:
        print(alx, aly)
        dist = find_path(alx, aly, lox, loy, lista)
        if dist is None:
            continue
        minpath = min(dist, minpath)
    print(minpath)

def find_path(alx, aly, lox, loy, lista):
    x, y = alx, aly
    heap = list()
    heap.append((0, x, y))
    kayty = {(alx, aly)}
    while True:
        print(x, y)
        if len(heap) == 0:
            return None
        matka, x, y = heapq.heappop(heap)
        for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1)):
            xx = x + dx
            yy = y + dy
            if (not 0 <= xx < len(lista[0])) or (not 0 <= yy < len(lista)):
                continue
            if (xx, yy) in kayty:
                continue
            if ord(lista[yy][xx]) - ord(lista[y][x]) <= 1:
                heapq.heappush(heap, (matka + 1, xx, yy))
                kayty.add((xx, yy))
                if (xx, yy) == (lox, loy):
                    return matka + 1



def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(list(syote))
    return lista


if __name__ == "__main__":
    main()
