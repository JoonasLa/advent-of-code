import numpy as np


def main():
    lista = get_input()
    print(lista)
    print(len(lista))

    for i in range(len(lista)):
        lista[i] = (i, lista[i])
    for i in range(len(lista)):
        for j in range(len(lista)):
            if lista[j][0] == i:
                paik = j
                break
        num = lista[paik][1]
        if num == 0:
            continue
        lista = lista[paik+1:] + lista[:paik]
        npaik = num % len(lista)

        lista = lista[:npaik] + [(i, num)] + lista[npaik:]
        # print(lista)

    for i in range(len(lista)):
        if lista[i][1] == 0:
            break
    else:
        assert False
    tot = 0
    for ss in (1000, 2000, 3000):
        tot += lista[(i + ss) % len(lista)][1]
    print(lista)
    print(tot)


def main2():
    lista = get_input()
    print(lista)
    print(len(lista))

    decryptkey = 811589153
    for i in range(len(lista)):
        lista[i] = (i, lista[i] * decryptkey)
    for _ in range(10):
        for i in range(len(lista)):
            for j in range(len(lista)):
                if lista[j][0] == i:
                    paik = j
                    break
            num = lista[paik][1]
            if num == 0:
                continue
            lista = lista[paik+1:] + lista[:paik]
            npaik = num % len(lista)

            lista = lista[:npaik] + [(i, num)] + lista[npaik:]
            # print(lista)

    for i in range(len(lista)):
        if lista[i][1] == 0:
            break
    else:
        assert False
    tot = 0
    for ss in (1000, 2000, 3000):
        tot += lista[(i + ss) % len(lista)][1]
    print(lista)
    print(tot)



def get_input():
    lista = []
    print("Anna syöte:", end="")
    while True:
        syote = input()
        if syote == "":
            break
        lista.append(int(syote))
    return lista


if __name__ == "__main__":
    main2()
